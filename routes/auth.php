<?php

use App\Http\Controllers\ArticleController;
use Livewire\Volt\Volt;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\PitchController;
use App\Http\Controllers\TestimonialController;

Route::middleware('guest')->group(function () {
    Route::get('/', [HomepageController::class, 'index'])->name('home');

    Volt::route('register', 'pages.auth.register')
        ->name('register');

    Volt::route('/login', 'pages.auth.login')
        ->name('login');

    Volt::route('forgot-password', 'pages.auth.forgot-password')
        ->name('password.request');

    Volt::route('reset-password/{token}', 'pages.auth.reset-password')
        ->name('password.reset');
});

Route::middleware('auth')->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])
        ->middleware(['auth', 'verified'])
        ->name('dashboard');

    Route::get('/sports', [PitchController::class, 'allSports'])
        ->name('pitches.allSports');

    Route::get('/pitches', [PitchController::class, 'index'])
        ->middleware(['auth', 'verified'])
        ->name('pitches.index');

    Route::get('pitches/{sport}', [PitchController::class, 'sportsPitches'])
        ->name('pitches.sportsPitches');

    Route::get('pitches/pitch/{pitch}', [PitchController::class, 'show'])
        ->name('pitches.show');

    Route::get('pitches/reserve/{pitch}', [PitchController::class, 'createReservation'])
        ->name('pitches.reserve');

    Route::get('/news', [ArticleController::class, 'index'])
        ->name('news-articles.index');

    Route::get('/news/all', [ArticleController::class, 'allNews'])
        ->name('news-articles.all');

    Route::get('/news/{category}', [ArticleController::class, 'showPostsByCategory'])
        ->name('news-articles.categories');

    Route::get('news/article/{post}', [ArticleController::class, 'show'])
        ->name('news.article.show');

    Route::get('/testimonial', TestimonialController::class)->name('review');

    Volt::route('verify-email', 'pages.auth.verify-email')
        ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)
        ->middleware(['signed', 'throttle:6,1'])
        ->name('verification.verify');

    Volt::route('confirm-password', 'pages.auth.confirm-password')
        ->name('password.confirm');
});
