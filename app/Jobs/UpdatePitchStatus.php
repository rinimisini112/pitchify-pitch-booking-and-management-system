<?php

namespace App\Jobs;

use App\Models\Pitch;
use App\Models\PitchWorkingSchedule;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Facades\Log;

class UpdatePitchStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }
    public function handle()
    {
        $pitches = PitchWorkingSchedule::all();

        foreach ($pitches as $pitch) {
            $currentTime = now()->format('H:i:s');

            if (!$this->isWorkingToday($pitch)) {
                $pitch->update(['is_closed' => 1]);
            } else {
                $worksFrom = $pitch->works_from;
                $worksUntil = $pitch->works_until;

                if ($worksUntil < $worksFrom) {

                    $isClosed = $currentTime < $worksFrom && $currentTime > $worksUntil;
                } else {
                    $isClosed = $currentTime < $worksFrom || $currentTime > $worksUntil;
                }

                $pitch->update(['is_closed' => $isClosed ? 1 : 0]);
            }
        }
    }
    protected function isWorkingToday($pitch)
    {
        $today = Carbon::now()->dayOfWeek;
        $workingDays = $pitch->working_days;

        $optionToDays = [
            0 => [0, 1, 2, 3, 4, 5, 6],   // Every Day
            1 => [1, 2, 3, 4, 5, 6],      // Monday to Saturday
            2 => [1, 2, 3, 4, 5],         // Monday to Friday
            3 => [1, 2, 3, 4],            // Monday to Thursday
            4 => [1, 2, 3],               // Monday to Wednesday
        ];

        $selectedDays = $optionToDays[$workingDays] ?? [];

        return in_array($today, $selectedDays);
    }
}
