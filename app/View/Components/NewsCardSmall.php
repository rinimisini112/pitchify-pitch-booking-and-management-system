<?php

namespace App\View\Components;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class NewsCardSmall extends Component
{
    public string $imageUrl;
    public string $newsTitle;
    public string $articleLink;
    public $newsTimestamp;
    /**
     * Create a new component instance.
     */
    public function __construct(string $imageUrl, string $articleLink, string $newsTitle, $newsTimestamp)
    {
        $this->imageUrl = $imageUrl;
        $this->articleLink = $articleLink;
        $this->newsTitle = $newsTitle;
        $this->newsTimestamp = $newsTimestamp;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.news-card-small');
    }
}
