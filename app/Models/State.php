<?php

namespace App\Models;

use App\Models\City;
use App\Models\Pitch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class State extends Model
{
    use HasFactory;

    protected $fillable = [
        'state_name'
    ];

    public $timestamps = false;

    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }
    public function pitches(): HasMany
    {
        return $this->hasMany(Pitch::class);
    }
}
