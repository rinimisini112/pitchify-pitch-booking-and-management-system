<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'thumbnail',
        'content',
        'active',
        'published_at',
        'tags',
        'category_id',
        'heading_id'
    ];

    protected $casts = [
        'tags' => 'array',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public static function getPostsForDashboard(): Collection
    {
        $posts = Post::select('title', 'slug', 'thumbnail')
            ->selectRaw('DATE_FORMAT(published_at, "%d/%m/%Y - %l:%i %p") as formatted_published_at')
            ->where('published_at', '<', now())
            ->orderBy('published_at', 'desc')
            ->take(4)
            ->get();

        return $posts;
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            if ($post->heading_id) {
                static::where('heading_id', $post->heading_id)->update(['heading_id' => 1]);
            }
        });

        static::updating(function ($post) {
            if ($post->heading_id) {
                // Unset heading_id from other posts
                static::where('heading_id', $post->heading_id)->update(['heading_id' => 1]);
            }
        });
    }
    public function heading(): BelongsTo
    {
        return $this->belongsTo(PostsHeading::class, 'heading_id');
    }
}
