<?php

namespace App\Models;

use App\Models\Pitch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sport extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'thumbnail'];

    public function pitches(): HasMany
    {
        return $this->hasMany(Pitch::class);
    }
}
