<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RatingPitch extends Model
{
    use HasFactory;
    protected $fillable = [
        'pitch_id',
        'user_id',
        'rating_id',
        'comment',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function pitch(): BelongsTo
    {
        return $this->belongsTo(Pitch::class);
    }

    public function rating(): BelongsTo
    {
        return $this->belongsTo(Rating::class);
    }
}
