<?php

namespace App\Models;

use App\Models\Pitch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = ['pitch_id', 'start_time', 'end_time'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function pitch()
    {
        return $this->belongsTo(Pitch::class);
    }

    public function pitchWorkingSchedule()
    {
        return $this->belongsTo(PitchWorkingSchedule::class);
    }
}
