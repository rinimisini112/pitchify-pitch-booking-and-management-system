<?php

namespace App\Models;

use App\Models\City;
use App\Models\Sport;
use App\Models\State;
use App\Models\RatingPitch;
use App\Models\Reservation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Pitch extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'images',
        'address',
        'price_for_hour',
        'dimension',
        'pitch_type',
        'player_limit',
        'sport_id',
        'state_id',
        'city_id',
        'description'
    ];

    protected $casts = [
        'images' => 'array',
    ];

    public function sports(): BelongsTo
    {
        return $this->belongsTo(Sport::class,  'sport_id');
    }

    public function reservations(): HasMany
    {
        return $this->hasMany(ReservationPitch::class);
    }

    public function ratingPitches(): HasMany
    {
        return $this->hasMany(RatingPitch::class);
    }
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }
    public function pitchWorkingSchedule(): HasOne
    {
        return $this->hasOne(PitchWorkingSchedule::class);
    }

    public function isAvailableForReservation($startTime, $endTime)
    {
        $workingSchedule = $this->workingSchedules->first(function ($schedule) use ($startTime, $endTime) {
            return $schedule->isWorkingDuring($startTime, $endTime);
        });

        if (!$workingSchedule) {
            return false;
        }

        $reservations = $this->reservations->filter(function ($reservation) use ($startTime, $endTime) {
            return $reservation->doesOverlap($startTime, $endTime);
        });

        return $reservations->isEmpty();
    }
}
