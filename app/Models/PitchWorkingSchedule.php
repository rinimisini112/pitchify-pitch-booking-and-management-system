<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PitchWorkingSchedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'pitch_id',
        'working_days',
        'works_from',
        'works_until',
        'is_closed'
    ];

    public function pitch(): BelongsTo
    {
        return $this->belongsTo(Pitch::class);
    }
}
