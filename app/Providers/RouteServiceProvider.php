<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\Sport;
use App\Models\Category;
use App\Models\Pitch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        Route::bind('sport', function ($value) {
            return Sport::where('slug', $value)->firstOrFail();
        });

        Route::bind('pitch', function ($value) {
            return Pitch::where('slug', $value)->firstOrFail();
        });

        Route::bind('category', function ($value) {
            return Category::with('posts')->where('slug', $value)->firstOrFail();
        });

        Route::bind('post', function ($value) {
            return Post::where('slug', $value)->firstOrFail();
        });
        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }
}
