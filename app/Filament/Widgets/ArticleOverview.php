<?php

namespace App\Filament\Widgets;

use App\Models\Pitch;
use App\Models\Reservation;
use Filament\Widgets\StatsOverviewWidget\Stat;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class ArticleOverview extends BaseWidget
{
    protected static ?string $pollingInterval = '10s';

    protected function getStats(): array
    {
        return [
            Stat::make('Total Pitches in Website', Pitch::all()->count()),
            Stat::make('Total Revenue', '20k')
                ->description('32k increase')
                ->descriptionIcon('heroicon-m-arrow-trending-up')
                ->chart([7, 2, 10, 3, 15, 4, 17])
                ->color('success'),
            Stat::make('Total Reservations so far', Reservation::all()->count()),
            Stat::make('Average time on page', '3:12'),
        ];
    }
}
