<?php

namespace App\Filament\Resources;

use App\Filament\Resources\RatingPitchResource\Pages;
use App\Filament\Resources\RatingPitchResource\RelationManagers;
use App\Models\RatingPitch;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class RatingPitchResource extends Resource
{
    protected static ?string $model = RatingPitch::class;

    protected static ?string $navigationIcon = 'heroicon-o-star';

    protected static ?string $navigationGroup = 'Pitches Management';

    protected static ?string $modelLabel = 'Rating';

    protected static ?int $navigationSort = 4;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListRatingPitches::route('/'),
            'create' => Pages\CreateRatingPitch::route('/create'),
            'edit' => Pages\EditRatingPitch::route('/{record}/edit'),
        ];
    }
}
