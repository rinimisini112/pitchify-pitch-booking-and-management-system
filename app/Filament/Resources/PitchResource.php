<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\City;
use App\Models\User;
use Filament\Tables;
use App\Models\Pitch;
use App\Models\State;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Illuminate\Support\Str;
use Termwind\Components\Raw;
use Filament\Facades\Filament;
use Filament\Resources\Resource;
use Filament\Forms\Components\Grid;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Model;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Filament\Tables\Columns\TextInputColumn;
use Illuminate\Database\Eloquent\Collection;
use Filament\Tables\Columns\Summarizers\Range;
use App\Filament\Resources\PitchResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\PitchResource\RelationManagers;
use App\Filament\Resources\Columns\ReadableWorkingHoursColumn;
use App\Tables\Columns\ReadableWorkingHoursColumn as ColumnsReadableWorkingHoursColumn;

class PitchResource extends Resource
{
    protected static ?string $model = Pitch::class;

    protected static ?string $navigationIcon = 'heroicon-o-squares-2x2';

    protected static ?string $navigationGroup = 'Pitches Management';

    protected static ?int $navigationSort = 2;

    public static function canViewAny(): bool
    {
        return auth()->user()->role > 1;
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make('Pitch Name and Sport it belongs to')
                    ->schema([
                        Forms\Components\TextInput::make('title')
                            ->required()
                            ->maxLength(2048)
                            ->columnSpan(3)
                            ->reactive()
                            ->debounce()
                            ->afterStateUpdated(fn ($state, callable $set) => $set('slug', Str::slug($state))),
                        Forms\Components\TextInput::make('slug')
                            ->required()
                            ->readOnly()
                            ->columnSpan(3)
                            ->maxLength(2048),
                        Forms\Components\Select::make('sport_id')
                            ->required()
                            ->relationship('sports', 'title')
                            ->label('Sport which is played on the pitch')
                            ->columnSpan(3),
                    ])->columns(6),
                Forms\Components\Section::make('Pitch Information')
                    ->schema([
                        Forms\Components\TextInput::make('price_for_hour')
                            ->required()
                            ->maxLength(2048)
                            ->numeric(),
                        Forms\Components\TextInput::make('dimension')
                            ->required()
                            ->maxLength(2048)
                            ->datalist([
                                'Proffesional Soccer(110x70m)',
                                'Casual Soccer(60x30m)',
                                'Proffesional Basketball(30x15m)',
                                'Proffesional Tennis(24x11m)',
                                'Standard Billiards Table(262x150cm)',
                                'Standard Ping Pong Table(275x152cm)',
                            ]),
                        Forms\Components\TextInput::make('pitch_type')
                            ->required()
                            ->maxLength(2048)
                            ->datalist([
                                'Artifical Grass(Turf)',
                                'Natural Grass',
                                'Indoor Basketball Court(Wood)',
                                'Outdoor Basketball Court(Concrete)',
                                'Clay',
                                'Standard Billiards Table',
                                'Standard Ping Pong Table',
                            ]),
                        Forms\Components\TextInput::make('player_limit')
                            ->required()
                            ->maxLength(2048)
                            ->datalist([
                                'Standart Soccer 11v11(22 Max)',
                                'Casual Soccer 6v6 (12-14 Max)',
                                'Standard Basketball 5v5(10-12 Max)',
                                'Standard Tennis 1v1 (2 Max)',
                                'Standard Billiards 1v1 / 2v2 (4-5 Max)',
                                'Standard Ping Pong 1v1 / 2v2 (4 Max)',
                            ]),
                    ])->columns(2),
                Forms\Components\Section::make('Pitch Address, State and City')
                    ->description('Upon selecting a state all the available cities for that state will appear!')
                    ->schema([
                        Forms\Components\Select::make('state_id')
                            ->label('District of')
                            ->required()
                            ->relationship('state', 'state_name')
                            ->live()
                            ->afterStateUpdated(fn (Set $set) => $set('city_id', null))
                            ->searchable()
                            ->preload(),
                        Forms\Components\Select::make('city_id')
                            ->required()
                            ->label('Neighborhood / Village')
                            ->options(fn (Get $get) => City::query()
                                ->where('state_id', $get('state_id'))
                                ->pluck('city_name', 'id'))
                            ->searchable()
                            ->live()
                            ->preload(),
                        Forms\Components\TextInput::make('address')
                            ->required()
                            ->maxLength(2048)
                            ->columnSpanFull(),
                    ])->columns(2),
                Forms\Components\Section::make('Pitch Content / Description')
                    ->schema([
                        Forms\Components\RichEditor::make('description')
                            ->required(),
                    ]),
                Forms\Components\Section::make('Pitch Images')
                    ->schema([
                        Forms\Components\FileUpload::make('images')->multiple(),
                    ]),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('images')
                    ->searchable()
                    ->limit(1)
                    ->width(120)
                    ->height(70),
                Tables\Columns\TextColumn::make('title')
                    ->searchable(),
                Tables\Columns\TextColumn::make('sports.title')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('price_for_hour')
                    ->money('eur')
                    ->searchable(),
                Tables\Columns\TextColumn::make('address')
                    ->searchable(),
                Tables\Columns\TextColumn::make('city_id')
                    ->searchable()
                    ->formatStateUsing(function ($state, Pitch $pitch) {
                        $city = $pitch->city->city_name;
                        $state = $pitch->state->state_name;
                        $city_and_state = "{$city}, {$state}";

                        return $city_and_state;
                    })
                    ->label('District and Municipality'),
            ])
            ->filters([])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPitches::route('/'),
            'create' => Pages\CreatePitch::route('/create'),
            'edit' => Pages\EditPitch::route('/{record}/edit'),
        ];
    }
}
