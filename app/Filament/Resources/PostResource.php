<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\Post;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Illuminate\Support\Str;
use Filament\Resources\Resource;
use Filament\Forms\Components\Grid;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\PostResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\PostResource\RelationManagers;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $navigationIcon = 'heroicon-o-newspaper';

    protected static ?string $label = 'News Article';

    protected static ?string $navigationGroup = 'Content Writting';



    public static function canViewAny(): bool
    {
        return auth()->user()->role == 1 || auth()->user()->role == 3;
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make('News Article Title')
                    ->schema([
                        Forms\Components\TextInput::make('title')
                            ->required()
                            ->placeholder('Make it fancy')
                            ->maxLength(2048)
                            ->label('Enter the news article title')
                            ->reactive()
                            ->debounce()
                            ->afterStateUpdated(fn ($state, callable $set) => $set('slug', Str::slug($state))),
                        Forms\Components\TextInput::make('slug')
                            ->required()
                            ->readOnly()
                            ->maxLength(2048),
                    ]),
                Forms\Components\Section::make('Tags and Category')
                    ->schema([
                        Forms\Components\TagsInput::make('tags')
                            ->separator(',')
                            ->label('Tags / optional but prefered')
                            ->columnSpan(5),
                        Forms\Components\Select::make('category_id')
                            ->required()
                            ->relationship('category', 'title')
                            ->label('Which news category does it belong to')
                            ->columnSpan(3),
                    ])->columns(8),
                Forms\Components\Section::make('The articles content')
                    ->schema([
                        Forms\Components\RichEditor::make('content')
                            ->required()
                            ->columnSpanFull(),
                    ]),
                Forms\Components\Section::make('Thumbnail and Article status')
                    ->description('If published date is set in the future the article will not be posted until that specified date. Or if active is not turned ON the post will be created but will not appear on the feed, you can change this later if you have a specified date for publishing')
                    ->schema([
                        Forms\Components\FileUpload::make('thumbnail')
                            ->required()
                            ->label('Thumbnail appears on the headline')
                            ->columnSpan(6),
                        Forms\Components\DateTimePicker::make('published_at')
                            ->required()
                            ->label('Post Published Date')
                            ->columnSpan(6),
                        Forms\Components\Toggle::make('active')
                            ->required()
                            ->label('Post Activity status turn on to make active')
                            ->columnSpan(4),
                        Forms\Components\Select::make('is_heading')
                            ->relationship('heading', 'heading_name')
                            ->label('Make the post a heading')
                            ->columnSpan(8),

                    ])
                    ->columns(12),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('thumbnail'),
                Tables\Columns\TextColumn::make('heading.heading_name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('title')
                    ->searchable(),
                Tables\Columns\IconColumn::make('active')
                    ->boolean(),
                Tables\Columns\TextColumn::make('published_at')
                    ->dateTime()
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }
    public static function getWidgets(): array
    {
        return [];
    }
}
