<?php

namespace App\Filament\Resources\PostResource\Widgets;

use App\Models\Post;
use Filament\Facades\Filament;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;

class PostsOverview extends BaseWidget
{
    public function table(Table $table): Table
    {



        return $table
            ->query(
                Post::query()->where('category_id', 1)
            )
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->searchable(),
            ]);
    }
}
