<?php

namespace App\Filament\Resources\PitchWorkingScheduleResource\Pages;

use App\Filament\Resources\PitchWorkingScheduleResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListPitchWorkingSchedules extends ListRecords
{
    protected static string $resource = PitchWorkingScheduleResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
