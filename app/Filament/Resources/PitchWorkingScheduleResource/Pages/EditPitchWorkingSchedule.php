<?php

namespace App\Filament\Resources\PitchWorkingScheduleResource\Pages;

use App\Filament\Resources\PitchWorkingScheduleResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditPitchWorkingSchedule extends EditRecord
{
    protected static string $resource = PitchWorkingScheduleResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
