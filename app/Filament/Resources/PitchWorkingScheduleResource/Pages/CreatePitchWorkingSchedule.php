<?php

namespace App\Filament\Resources\PitchWorkingScheduleResource\Pages;

use App\Filament\Resources\PitchWorkingScheduleResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreatePitchWorkingSchedule extends CreateRecord
{
    protected static string $resource = PitchWorkingScheduleResource::class;
}
