<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Pitch;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Tables\Filters\Filter;
use App\Models\PitchWorkingSchedule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\PitchWorkingScheduleResource\Pages;
use App\Filament\Resources\PitchWorkingScheduleResource\RelationManagers;
use App\Models\Sport;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Filament\Tables\Filters\SelectFilter;

class PitchWorkingScheduleResource extends Resource
{
    protected static ?string $model = PitchWorkingSchedule::class;

    protected static ?string $navigationIcon = 'heroicon-o-clock';

    protected static ?string $navigationGroup = 'Pitches Management';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make('Enter a Pitches Working Schedule')
                    ->description('Upon creating a pitch this form should be filled after, otherwise the Pitch will not appear for reservation')
                    ->schema([
                        Forms\Components\Select::make('pitch_id')
                            ->options(Pitch::doesntHave('pitchWorkingSchedule')->pluck('title', 'id'))
                            ->required()
                            ->preload(),
                        Forms\Components\Select::make('working_days')
                            ->required()
                            ->options([
                                0 => 'Every Day',
                                1 => 'Monday-Saturday',
                                2 => 'Monday-Friday',
                                3 => 'Monday-Thursday',
                                4 => 'Monday-Wednesday',
                            ]),
                        Forms\Components\TimePicker::make('works_from')
                            ->required(),
                        Forms\Components\TimePicker::make('works_until')
                            ->required(),
                        Forms\Components\Toggle::make('is_closed')
                            ->label('Open or Closed Status, Changes automatically but in case of holidays or special cases, you may toggle it off here!')
                            ->required(),

                    ])->columns(2),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('pitch.title')
                    ->sortable()
                    ->label('Pitch Name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('pitch.sport_id')
                    ->searchable()
                    ->formatStateUsing(function ($state, Sport $sport) {
                        $sport = Sport::where('id', $state)->first();

                        return $sport ? $sport->title : '';
                    })
                    ->label('Sport'),
                Tables\Columns\TextColumn::make('working_days')
                    ->formatStateUsing(function ($state, PitchWorkingSchedule $pitch) {
                        $workingDays = $pitch->working_days; // Assuming 'working_days' is the column in your database

                        switch ($workingDays) {
                            case 0:
                                return 'Every Day';
                            case 1:
                                return 'Monday-Saturday';
                            case 2:
                                return 'Monday-Friday';
                            case 3:
                                return 'Monday-Thursday';
                            case 4:
                                return 'Monday-Wednesday';
                            default:
                                return 'N/A';
                        }
                    }),
                Tables\Columns\TextColumn::make('works_from')
                    ->formatStateUsing(function ($state, PitchWorkingSchedule $pitch) {
                        $worksFrom = date('H:i', strtotime($pitch->works_from));
                        $worksUntil = date('H:i', strtotime($pitch->works_until));
                        return $worksFrom . ' to ' . $worksUntil;
                    }),
                Tables\Columns\TextColumn::make('is_closed')
                    ->searchable()
                    ->label('Status')
                    ->formatStateUsing(function ($state, PitchWorkingSchedule $schedule) {
                        return $schedule->is_closed ? 'Closed' : 'Opened';
                    })
                    ->color(function ($state, PitchWorkingSchedule $pitch) {
                        return $pitch->is_closed ? 'danger' : 'success';
                    }),
            ])
            ->filters([
                Filter::make('Is Opened')
                    ->query(fn (Builder $query): Builder => $query->where('is_closed', false)),
                SelectFilter::make('working_days')
                    ->options([
                        0 => 'Every Day',
                        1 => 'Monday-Saturday',
                        2 => 'Monday-Friday',
                        3 => 'Monday-Thursday',
                        4 => 'Monday-Wednesday',
                    ]),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPitchWorkingSchedules::route('/'),
            'create' => Pages\CreatePitchWorkingSchedule::route('/create'),
            'edit' => Pages\EditPitchWorkingSchedule::route('/{record}/edit'),
        ];
    }
}
