<?php

namespace App\Filament\Resources\RatingPitchResource\Pages;

use App\Filament\Resources\RatingPitchResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListRatingPitches extends ListRecords
{
    protected static string $resource = RatingPitchResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
