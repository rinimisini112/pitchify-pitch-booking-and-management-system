<?php

namespace App\Filament\Resources\RatingPitchResource\Pages;

use App\Filament\Resources\RatingPitchResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditRatingPitch extends EditRecord
{
    protected static string $resource = RatingPitchResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
