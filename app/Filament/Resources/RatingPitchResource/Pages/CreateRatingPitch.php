<?php

namespace App\Filament\Resources\RatingPitchResource\Pages;

use App\Filament\Resources\RatingPitchResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateRatingPitch extends CreateRecord
{
    protected static string $resource = RatingPitchResource::class;
}
