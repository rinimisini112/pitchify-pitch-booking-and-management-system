<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\RatingPitch;
use Illuminate\Support\Facades\Auth;

class RatingModal extends Component
{
    public $pitch;
    public $rating = 0;
    public $comment = '';

    protected $listeners = ['rating-form'];

    protected $rules = [
        'rating' => 'required|numeric|min:1|max:5',
        'comment' => 'nullable|string',
    ];

    public function submitRating()
    {
        $this->validate();

        $ratingPitches = RatingPitch::query()->create([
            'pitch_id' => $this->pitch->id,
            'rating_id' => $this->rating,
            'user_id' => Auth::user()->id,
            'comment' => $this->comment,
        ]);
        if ($ratingPitches) {
            $this->dispatch('close-modal', 'rating-form');
            $this->dispatch('ratingSubmitted', message: 'Thank you for submitting your review, here at Pitchify we take our customers seriously and every comment and rating matters to us.');
        }
    }

    public function mount($pitch)
    {
        $this->pitch = $pitch;
    }
    public function render()
    {
        return view('livewire.rating-modal');
    }
}
