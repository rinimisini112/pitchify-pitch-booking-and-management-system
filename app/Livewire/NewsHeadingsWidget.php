<?php

namespace App\Livewire;

use App\Models\Post;
use Livewire\Component;

class NewsHeadingsWidget extends Component
{
    public $mainPost;
    public $mediumPost;
    public $smallPost1;
    public $smallPost2;

    public function allHeadingsSet()
    {
        return isset($this->mainPost, $this->mediumPost, $this->smallPost1, $this->smallPost2);
    }

    public function mount()
    {
        $this->mainPost = Post::where('heading_id', 2)
            ->where('active', 1)
            ->first();
        $this->mediumPost = Post::where('heading_id', 3)
            ->where('active', 1)
            ->first();
        $this->smallPost1 = Post::where('heading_id', 4)
            ->where('active', 1)
            ->first();
        $this->smallPost2 = Post::where('heading_id', 5)
            ->where('active', 1)
            ->first();
    }

    public function render()
    {

        return view('livewire.news-headings-widget');
    }
}
