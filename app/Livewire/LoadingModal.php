<?php

namespace App\Livewire;

use Livewire\Component;

class LoadingModal extends Component
{
    public function mount()
    {
        $this->dispatch('closeLoadingModal');
    }
    public function render()
    {
        return view('livewire.loading-modal');
    }
}
