<?php


namespace App\Livewire;

use Livewire\Component;


class NotificationPopup extends Component
{
    public $show = false;
    public $message;

    protected $listeners = ['showNotification'];

    public function showNotification($message)
    {
        $this->show = true;
        $this->message = $message;

        $this->dispatch('hideNotification', ['timeout' => 6000]);
    }

    public function render()
    {
        return view('livewire.notification-popup');
    }
}
