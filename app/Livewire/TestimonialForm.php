<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Testimonial;

class TestimonialForm extends Component
{
    public string $content;

    protected $rules = [
        'content' => 'required|min:10',
    ];

    public function render()
    {
        return view('livewire.testimonial-form');
    }

    public function saveTestimonial()
    {
        $this->validate();

        Testimonial::create([
            'user_id' => auth()->user()->id,
            'content' => $this->content,
        ]);

        $this->dispatch('showNotification', 'Thank you for submitting your review. We always stay in touch with our users.');
        $this->redirect('/dashboard', true);
    }
}
