<?php

namespace App\Livewire;

use App\Models\Pitch;
use Livewire\Component;
use Illuminate\Support\Str;

class PitchCardMd extends Component
{
    public $pitch;

    public function mount(Pitch $pitch)
    {
        $this->pitch = $pitch;
    }

    public function render()
    {
        $images = $this->pitch->images;
        $first_image = $images[0];
        $second_image = $images[1] ?? null;

        $rating_count = $this->pitch->ratingPitches->count();

        $sum_of_ratings = $this->pitch->ratingPitches->sum('rating_id');

        $average_rating = $rating_count > 0 ? $sum_of_ratings / $rating_count : 0;
        return view('livewire.pitch-card-md', compact('first_image', 'second_image', 'average_rating'));
    }
}
