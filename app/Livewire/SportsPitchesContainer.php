<?php

namespace App\Livewire;

use App\Models\Pitch;
use App\Models\Sport;
use Livewire\Component;
use Livewire\WithPagination;

class SportsPitchesContainer extends Component
{
    use WithPagination;

    public $search = '';
    public $sport;
    public ?bool $toggleOpen = false;

    public function mount(Sport $sport)
    {
        $this->sport = $sport;
    }

    public function render()
    {
        $searchTerm = strtolower(trim($this->search));

        $query = Pitch::where('sport_id', $this->sport->id)
            ->where(function ($query) use ($searchTerm) {
                $query->where('title', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('slug', 'LIKE', "%{$searchTerm}%");
            });

        if ($this->toggleOpen) {
            $query->whereHas('pitchWorkingSchedule', function ($subquery) {
                $subquery->where('is_closed', 0);
            });
        };

        $pitches = $query->paginate(4);

        $this->resetPage();
        return view('livewire.sports-pitches-container', compact('pitches'));
    }
}
