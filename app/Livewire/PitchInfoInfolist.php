<?php

namespace App\Livewire;

use Livewire\Component;

class PitchInfoInfolist extends Component
{
    public $pitch;
    public $average_rating;

    public $successMessage = '';
    public $showSuccessMessage = false;

    protected $listeners = ['ratingSubmitted' => 'showSuccessMessage'];

    public function showSuccessMessage($message)
    {
        $this->successMessage = $message;
        $this->showSuccessMessage = true;
    }
    public function ratingSubmitted()
    {
        $this->render();
    }
    public function mount($pitch, $average_rating)
    {
        $this->pitch = $pitch;
        $this->average_rating = $average_rating;
    }

    public function render()
    {
        return view('livewire.pitch-info-infolist');
    }
}
