<?php

namespace App\Livewire;

use App\Models\Post;
use App\Models\User;
use Filament\Actions\Action;
use Filament\Forms\Components\Actions\Action as ActionsAction;
use Filament\Forms\Set;
use Livewire\Component;
use Filament\Forms\Form;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Section;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Notifications\Notification;

class UserProfileForm extends Component implements HasForms
{
    use InteractsWithForms;
    public ?array $data = [];

    public User $user;

    public function mount(User $user): void
    {
        $this->form->fill($user->toArray());
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make('Update your profile')
                    ->schema([
                        TextInput::make('name')
                            ->required()
                            ->label('Your Full Name')
                            ->columnSpan(3),
                        TextInput::make('email')
                            ->label('E-Mail')
                            ->required()
                            ->columnSpan(3),
                        FileUpload::make('profile_picture')
                            ->avatar()
                            ->columnSpan(2),
                    ])->columns(8),
            ])
            ->statePath('data')
            ->model(User::class);
    }

    public function update(): void
    {

        $user = User::find(auth()->user()->id);

        $user->update($this->form->getState());


        $this->dispatch('showNotification', 'Profile updated succesfully');
    }
    public function render()
    {
        return view('livewire.user-profile-form');
    }
}
