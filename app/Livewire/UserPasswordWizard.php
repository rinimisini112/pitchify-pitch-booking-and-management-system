<?php

namespace App\Livewire;

use App\Models\User;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Livewire\Component;
use Filament\Forms\Form;
use Illuminate\Support\HtmlString;
use Filament\Forms\Components\Grid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Filament\Forms\Components\Wizard;
use Illuminate\Support\Facades\Blade;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Illuminate\Validation\ValidationException;
use Filament\Forms\Concerns\InteractsWithForms;

class UserPasswordWizard extends Component implements HasForms
{
    use InteractsWithForms;

    public ?array $data = [];

    public User $user;


    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Wizard::make([
                    Wizard\Step::make('Enter your old password')
                        ->schema([
                            TextInput::make('password')
                                ->required()
                                ->password()
                        ])
                        ->afterValidation(function ($component) {
                            $current_password = $component->getChildComponents()[0]->getState();
                            if (!Auth::attempt(['email' => Auth::user()->email, 'password' => $current_password])) {
                                throw ValidationException::withMessages([
                                    $component->getChildComponents()[0]->getStatePath() => 'The password you provided is incorrect!'
                                ]);
                            }
                        }),
                    Wizard\Step::make('Enter new password')
                        ->schema([
                            TextInput::make('new_password')
                                ->label('Enter your new password')
                                ->password()
                                ->confirmed()
                                ->live()
                                ->minLength(8)
                                ->required(),
                            TextInput::make('new_password_confirmation')
                                ->label('Confirm your new password')
                                ->required()
                                ->password(),
                        ])->columns(2),
                ])->submitAction(new HtmlString(Blade::render(<<<BLADE
                <x-filament::button
                    type="submit"
                    size="sm"
                    style="border-radius: 0 !important; color:#ffca28;"
                    class=" px-8 py-2 bg-white font-bold tracking-wide rounded-none duration-150 hover:bg-transparent hover:!text-white  hover:ring hover:ring-white active:ring-8"
                >
                    Submit
                </x-filament::button>
            BLADE)))
            ])
            ->statePath('data')
            ->model(User::class);
    }

    public function update(): void
    {

        $user = Auth::user();
        $oldPassword = $this->form->getState()['password'];

        if (!Hash::check($oldPassword, $user->password)) {
            throw new \Illuminate\Validation\ValidationException(['old_password' => __('validation.password')]);
        }

        $newPassword = $this->form->getState()['new_password'];


        Auth::user()->update([
            'password' => Hash::make($newPassword),
        ]);

        $this->dispatch('showNotification', 'Password updated successfully');
    }

    public function render()
    {
        return view('livewire.user-password-wizard');
    }
}
