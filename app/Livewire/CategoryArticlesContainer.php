<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class CategoryArticlesContainer extends Component
{
    use WithPagination;

    public $category;

    public function mount($category)
    {
        $this->category = $category;
    }

    private function getPostsByCategory(Category $category)
    {
        return $category->posts()
            ->where('active', '=', 1)
            ->paginate(6);
    }

    public function render()
    {
        $posts = $this->getPostsByCategory($this->category);

        return view('livewire.category-articles-container', \compact('posts'));
    }
}
