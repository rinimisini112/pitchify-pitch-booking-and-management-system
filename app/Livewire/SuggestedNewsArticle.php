<?php

namespace App\Livewire;

use App\Models\Post;
use Livewire\Component;
use App\Models\Category;

class SuggestedNewsArticle extends Component
{
    public $posts;
    public $category;

    public function mount(Category $category)
    {
        $this->category = $category;
        $this->loadPosts();
    }

    public function loadPosts()
    {
        // Assuming 'posts' is the relationship method in the Category model
        $this->posts = $this->category->posts()->take(2)->get();
    }

    public function render()
    {
        return view('livewire.suggested-news-article');
    }
}
