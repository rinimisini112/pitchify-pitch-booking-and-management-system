<?php

namespace App\Livewire;

use DateTime;
use DateInterval;
use App\Models\Pitch;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Livewire\Component;
use Filament\Forms\Form;
use Ramsey\Uuid\Type\Time;
use Illuminate\Support\Carbon;
use App\Models\ReservationPitch;
use Illuminate\Support\HtmlString;
use Filament\Forms\Components\Grid;
use Illuminate\Support\Facades\Auth;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Wizard;
use Illuminate\Support\Facades\Blade;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\TimePicker;
use Filament\Forms\Components\DateTimePicker;
use Filament\Forms\Concerns\InteractsWithForms;
use Coolsam\FilamentFlatpickr\Forms\Components\Flatpickr;
use HusamTariq\FilamentTimePicker\Forms\Components\TimePickerField;

class ReservationWizard extends Component implements HasForms
{
    use InteractsWithForms;

    public Pitch $pitch;
    public $startTimes = [];
    public ?array $data = [];

    public function getWorkingDaysDate()
    {
        $workingDays = $this->pitch->pitchWorkingSchedule->working_days;
        $currentDate = Carbon::now();
        $nonWorkingDays = [];

        $dayMapping = [
            0 => [],
            1 => [Carbon::SUNDAY],
            2 => [Carbon::SUNDAY, Carbon::SATURDAY],
            3 => [Carbon::SUNDAY, Carbon::SATURDAY, Carbon::FRIDAY],
            4 => [Carbon::SUNDAY, Carbon::SATURDAY, Carbon::FRIDAY, Carbon::THURSDAY],
        ];

        $excludedDays = $dayMapping[$workingDays] ?? [];

        for ($i = 0; $i < 4; $i++) {
            $daysInMonth = $currentDate->daysInMonth;

            for ($day = 1; $day <= $daysInMonth; $day++) {
                $currentDate->day($day);

                if (in_array($currentDate->dayOfWeek, $excludedDays)) {
                    $nonWorkingDays[] = $currentDate->toDateString();
                }
            }

            $currentDate->addMonthsWithNoOverflow();
        }

        return $nonWorkingDays;
    }

    public function calculateTimeOptions()
    {
        $startTime = Carbon::parse($this->pitch->pitchWorkingSchedule->works_from);
        $endTime = Carbon::parse($this->pitch->pitchWorkingSchedule->works_until)->subHour();

        $timeOptions = [];

        while ($startTime <= $endTime) {
            $timeOptions[$startTime->format('H:i')] = $startTime->format('h:i A');
            $startTime->addHour(); // Increment by 1 hour
        }

        return $timeOptions;
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Wizard::make([
                    Wizard\Step::make('Select Date and Start and End time for your reservation')
                        ->schema([
                            Grid::make(2)
                                ->schema([
                                    DatePicker::make('date')
                                        ->prefix('Click to pick a date')
                                        ->label('Select your date')
                                        ->native(false)
                                        ->closeOnDateSelection()
                                        ->minDate(now())
                                        ->maxDate(now()->addMonths(3))
                                        ->columnSpan(2)
                                        ->disabledDates($this->getWorkingDaysDate()),
                                    Select::make('start_time')
                                        ->options(function () {
                                            return $this->startTimes;
                                        })
                                        ->native(\false)

                                        ->afterStateUpdated(fn (Set $set) => $set('end_time', null))
                                        ->live(),
                                    Select::make('end_time')
                                        ->live()
                                        ->native(\false)
                                        ->options(function () {
                                            return $this->startTimes;
                                        }),
                                ])
                        ]),
                    Wizard\Step::make('Enter new password')
                        ->schema([])->columns(2),
                ])->submitAction(new HtmlString(Blade::render(<<<BLADE
                <x-filament::button
                    type="submit"
                    size="sm"
                    style="border-radius: 0 !important; color:#ffca28;"
                    class=" px-8 py-2 bg-white font-bold tracking-wide rounded-none duration-150 hover:bg-transparent hover:!text-white  hover:ring hover:ring-white active:ring-8"
                >
                    Submit
                </x-filament::button>
            BLADE)))
            ])
            ->statePath('data')
            ->model(ReservationPitch::class);
    }
    protected function getFormModel(): string
    {
        return ReservationPitch::class;
    }
    public function mount($pitch)
    {
        $this->form->fill();
        $this->pitch = $pitch;
        $this->startTimes = $this->calculateTimeOptions();
    }

    public function render()
    {
        return view('livewire.reservation-wizard');
    }
}
