<?php

namespace App\Livewire;

use App\Models\Sport;
use Livewire\Component;

class SearchAllSports extends Component
{
    public $query = '';
    public $results = [];

    public function updatedQuery()
    {
    }

    public function render()
    {
        $searchTerm = strtolower(trim($this->query));


        $this->results = Sport::where('title', 'LIKE', "%{$searchTerm}%")
            ->orWhere('slug', 'LIKE', "%{$searchTerm}%")
            ->get();


        return view('livewire.search-all-sports', ['results' => $this->results]);
    }
}
