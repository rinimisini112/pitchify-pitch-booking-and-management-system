<?php

namespace App\Livewire;

use App\Models\Post;
use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class ArticleContainer extends Component
{
    use WithPagination;


    private function getAllNews()
    {
        return Post::query()
            ->select('title', 'slug', 'thumbnail', 'published_at')
            ->where('active', 1)
            ->orderBy('published_at', 'desc')->paginate(8);
    }



    public function render()
    {
        $posts = $this->getAllNews();

        return view('livewire.article-container', \compact('posts'));
    }
}
