<?php

namespace App\Livewire;

use App\Models\Sport;
use Livewire\Component;

class SportsSidebar extends Component
{
    public $sports;

    public function mount($sports)
    {
        $this->sports = $sports;
    }

    public function render()
    {
        return view('livewire.sports-sidebar');
    }
}
