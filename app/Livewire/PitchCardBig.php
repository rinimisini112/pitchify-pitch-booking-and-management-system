<?php

namespace App\Livewire;

use App\Models\Pitch;
use Livewire\Component;
use Illuminate\Support\Str;

class PitchCardBig extends Component
{
    public $pitch;

    public function mount(Pitch $pitch)
    {
        $this->pitch = $pitch;
    }
    protected function extractValueBetweenParentheses($text)
    {
        $insideValue = Str::between($text, '(', ')');
        $outsideValue = trim(Str::before($text, '('));

        return [
            'inside' => $insideValue,
            'outside' => $outsideValue,
        ];
    }

    public function render()
    {
        $images = $this->pitch->images;
        $first_image = $images[0];
        $second_image = $images[1] ?? null;

        $rating_count = $this->pitch->ratingPitches->count();

        $sum_of_ratings = $this->pitch->ratingPitches->sum('rating_id');

        $average_rating = $rating_count > 0 ? $sum_of_ratings / $rating_count : 0;

        $dimensionValues = $this->extractValueBetweenParentheses($this->pitch->dimension);
        $grassTypeValues = $this->extractValueBetweenParentheses($this->pitch->pitch_type);
        $maxPlayers = $this->extractValueBetweenParentheses($this->pitch->player_limit);

        return view('livewire.pitch-card-big', compact('first_image', 'second_image', 'dimensionValues', 'average_rating', 'grassTypeValues', 'maxPlayers'));
    }
}
