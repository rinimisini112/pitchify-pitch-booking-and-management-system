<?php

namespace App\Livewire;

use App\Models\Pitch;
use App\Models\Sport;
use Livewire\Component;
use Livewire\WithPagination;

class PitchContainer extends Component
{
    use WithPagination;

    public string $search = '';
    public ?bool $toggleOpen = false;

    public function search()
    {
    }

    public function render()
    {

        $searchTerm = strtolower(trim($this->search));

        $query = Pitch::where('title', 'LIKE', "%{$searchTerm}%")
            ->orWhere('slug', 'LIKE', "%{$searchTerm}%");

        if ($this->toggleOpen) {
            $query->whereHas('pitchWorkingSchedule', function ($subquery) {
                $subquery->where('is_closed', 0);
            });
        }

        $pitches = $query->paginate(4);

        $this->resetPage();

        return view('livewire.pitch-container', compact('pitches'));
    }
}
