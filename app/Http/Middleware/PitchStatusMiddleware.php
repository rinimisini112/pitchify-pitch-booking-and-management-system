<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PitchWorkingSchedule;
use Symfony\Component\HttpFoundation\Response;

class PitchStatusMiddleware
{
    public function handle($request, Closure $next)
    {
        $pitches = PitchWorkingSchedule::all();

        foreach ($pitches as $pitch) {
            $currentTime = now()->format('H:i:s');

            if (!$this->isWorkingToday($pitch)) {
                $pitch->update(['is_closed' => 1]);
            } else {
                $worksFrom = $pitch->works_from;
                $worksUntil = $pitch->works_until;

                if ($worksUntil < $worksFrom) {
                    $isClosed = $currentTime < $worksFrom && $currentTime > $worksUntil;
                } else {
                    $isClosed = $currentTime < $worksFrom || $currentTime > $worksUntil;
                }

                $pitch->update(['is_closed' => $isClosed ? 1 : 0]);
            }
        }

        return $next($request);
    }

    protected function isWorkingToday($pitch)
    {
        $today = Carbon::now()->dayOfWeek;
        $workingDays = $pitch->working_days;

        $optionToDays = [
            0 => [0, 1, 2, 3, 4, 5, 6],   // Every Day
            1 => [1, 2, 3, 4, 5, 6],      // Monday to Saturday
            2 => [1, 2, 3, 4, 5],         // Monday to Friday
            3 => [1, 2, 3, 4],            // Monday to Thursday
            4 => [1, 2, 3],               // Monday to Wednesday
        ];

        $selectedDays = $optionToDays[$workingDays] ?? [];

        return in_array($today, $selectedDays);
    }
}
