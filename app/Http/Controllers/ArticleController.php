<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Contracts\View\View;


class ArticleController extends Controller
{
    public function index(): View
    {
        $posts = Post::query()
            ->select('title', 'slug', 'thumbnail', 'published_at')
            ->where('active', '=', 1)
            ->orderBy('published_at', 'desc')
            ->take(4)
            ->get();


        $categories = Category::get();

        return view('app_views.news-articles.index', \compact('posts', 'categories'));
    }

    public function allNews()
    {
        $categories = Category::get();

        return view('app_views.news-articles.all', \compact('categories'));
    }

    public function showPostsByCategory(Category $category)
    {
        $categories = Category::get();

        return view('app_views.news-articles.categories-posts', \compact('categories', 'category'));
    }

    public function show(Post $post): View
    {
        return view('app_views.news-articles.show', compact('post'));
    }
}
