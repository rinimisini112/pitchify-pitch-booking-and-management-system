<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Pitch;
use App\Models\Post;
use App\Models\Sport;
use App\Models\Testimonial;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $sports = Sport::all(['title', 'thumbnail', 'slug'])->take(5);

        $pitches = Pitch::query()->select('pitches.*')
            ->leftJoin('rating_pitches', 'pitches.id', '=', 'rating_pitches.pitch_id')
            ->groupBy('pitches.id')
            ->orderByRaw('AVG(rating_pitches.rating_id) DESC')
            ->take(6)
            ->get();

        $categories = Category::all()->take(6);

        $posts = Post::query()
            ->where('active', '=', 1)
            ->orderBy('published_at', 'desc')
            ->take(3)
            ->get();

        $testimonials = Testimonial::query()
            ->orderBy('created_at', 'desc')
            ->take(4)
            ->get();

        return view('guest_views.home', \compact('sports', 'posts', 'categories', 'pitches', 'testimonials'));
    }
}
