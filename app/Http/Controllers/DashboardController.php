<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Post;
use App\Models\User;
use App\Models\Pitch;
use App\Models\Sport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $pitches = Pitch::all()->take(3);

        $posts = Post::getPostsForDashboard();

        $sports = Sport::select(['title', 'thumbnail', 'slug'])->get()->take(5);

        return \view('app_views.dashboard', \compact('pitches', 'posts', 'sports'));
    }
}
