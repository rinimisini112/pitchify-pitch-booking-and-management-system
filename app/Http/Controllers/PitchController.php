<?php

namespace App\Http\Controllers;

use App\Models\Pitch;
use App\Models\Sport;
use Illuminate\View\View;
use Illuminate\Http\Request;

class PitchController extends Controller
{
    public function index(): View
    {
        $sports = Sport::all();

        return \view('app_views.pitches.index', \compact('sports'));
    }

    public function show(Pitch $pitch): View
    {
        $sports = Sport::get();

        $rating_count = $pitch->ratingPitches->count();

        $sum_of_ratings = $pitch->ratingPitches->sum('rating_id');

        $average_rating = $rating_count > 0 ? $sum_of_ratings / $rating_count : 0;

        return \view('app_views.pitches.show', \compact('sports', 'pitch', 'average_rating'));
    }

    public function allSports(): View
    {
        return \view('app_views.pitches.sports');
    }

    public function sportsPitches(Sport $sport): View
    {
        $pitches = $sport->pitches;

        $sports = Sport::all();

        return view('app_views.pitches.sports_pitches', compact('sport', 'sports', 'pitches'));
    }

    public function createReservation(Pitch $pitch): View
    {
        return \view('app_views.pitches.reserve', \compact('pitch'));
    }
}
