<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pitch_working_schedules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pitch_id')->constrained();
            $table->unsignedTinyInteger('working_days');
            $table->time('works_from');
            $table->time('works_until');
            $table->boolean('is_closed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pitch_working_schedules');
    }
};
