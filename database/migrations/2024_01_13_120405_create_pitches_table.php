<?php

use App\Models\City;
use App\Models\Rating;
use App\Models\Sport;
use App\Models\State;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pitches', function (Blueprint $table) {
            $table->id();
            $table->string('title', 2048);
            $table->string('slug', 2048);
            $table->longText('description');
            $table->json('images')->nullable();
            $table->string('price_for_hour', 2048);
            $table->string('dimension', 2048);
            $table->string('pitch_type', 2048);
            $table->string('player_limit', 2048);
            $table->string('address', 2048);
            $table->foreignIdFor(City::class, 'city_id');
            $table->foreignIdFor(State::class, 'state_id');
            $table->foreignIdFor(Sport::class, 'sport_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pitches');
    }
};
