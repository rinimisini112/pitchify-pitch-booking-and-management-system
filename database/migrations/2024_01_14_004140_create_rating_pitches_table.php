<?php

use App\Models\Pitch;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rating_pitches', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Pitch::class, 'pitch_id');
            $table->foreignIdFor(Rating::class, 'rating_id');
            $table->foreignIdFor(User::class, 'user_id');
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rating_pitches');
    }
};
