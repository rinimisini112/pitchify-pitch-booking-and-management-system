<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('states')->insert([
            ['state_name' => 'Prishtina'],
            ['state_name' => 'Gjakova'],
            ['state_name' => 'Peja'],
            ['state_name' => 'Ferizaj'],
            ['state_name' => 'Prizren'],
            ['state_name' => 'Mitrovica'],
            ['state_name' => 'Gjilan'],
        ]);
    }
}
