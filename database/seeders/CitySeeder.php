<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('cities')->insert([
            ['city_name' => 'Ferizaj', 'state_id' => 1],
            ['city_name' => 'Hani i Elezit', 'state_id' => 1],
            ['city_name' => 'Kaçanik', 'state_id' => 1],
            ['city_name' => 'Shtime', 'state_id' => 1],
            ['city_name' => 'Štrpce', 'state_id' => 1],
            ['city_name' => 'Deçan', 'state_id' => 7],
            ['city_name' => 'Gjakova', 'state_id' => 7],
            ['city_name' => 'Junik', 'state_id' => 7],
            ['city_name' => 'Rahovec', 'state_id' => 7],
            ['city_name' => 'Gjilan', 'state_id' => 2],
            ['city_name' => 'Kamenica', 'state_id' => 2],
            ['city_name' => 'Klokot', 'state_id' => 2],
            ['city_name' => 'Partesh', 'state_id' => 2],
            ['city_name' => 'Ranilug', 'state_id' => 2],
            ['city_name' => 'Viti', 'state_id' => 2],
            ['city_name' => 'Leposavić', 'state_id' => 5],
            ['city_name' => 'Mitrovica', 'state_id' => 5],
            ['city_name' => 'North Mitrovica', 'state_id' => 5],
            ['city_name' => 'Skenderaj', 'state_id' => 5],
            ['city_name' => 'Vushtrri', 'state_id' => 5],
            ['city_name' => 'Zubin Potok', 'state_id' => 5],
            ['city_name' => 'Zveqan', 'state_id' => 5],
            ['city_name' => 'Drenas', 'state_id' => 4],
            ['city_name' => 'Gračanica', 'state_id' => 4],
            ['city_name' => 'Fushe Kosove', 'state_id' => 4],
            ['city_name' => 'Lipjan', 'state_id' => 4],
            ['city_name' => 'Novo Berrda', 'state_id' => 4],
            ['city_name' => 'Obiliq', 'state_id' => 4],
            ['city_name' => 'Podujevo', 'state_id' => 4],
            ['city_name' => 'Prishtina', 'state_id' => 4],
            ['city_name' => 'Peja', 'state_id' => 6],
            ['city_name' => 'Istog', 'state_id' => 6],
            ['city_name' => 'Klina', 'state_id' => 6],
            ['city_name' => 'Dragash', 'state_id' => 3],
            ['city_name' => 'Malisheva', 'state_id' => 3],
            ['city_name' => 'Mamusha', 'state_id' => 3],
            ['city_name' => 'Prizren', 'state_id' => 3],
            ['city_name' => 'Suarek', 'state_id' => 3],

        ]);
    }
}
