<div class="mt-6 w-[90%] border-4 border-white">

  <div class="group relative h-[400px] w-full border-b-4 border-white">
    <div
      class="bg-opacity-65 absolute right-0 top-0 z-30 border-b-4 border-l-4 border-white bg-white px-8 py-2 text-xl font-black backdrop-blur-sm">
      @if ($pitch->pitchWorkingSchedule->is_closed === 1)
        <p class="sports-night-ns text-center tracking-wide text-red-500">Closed</p>
      @else
        <p class="sports-night-ns text-center tracking-wide text-green-600">Open</p>
      @endif
    </div>
    <img class="h-full w-full object-cover" src="{{ asset('storage/' . $first_image) }}" alt="">
    @if ($second_image)
      <img
        class="absolute left-0 top-0 h-full w-full object-cover opacity-0 duration-200 ease-in group-hover:opacity-100"
        src="{{ asset('storage/' . $second_image) }}" alt="">
    @endif
  </div>
  <div class="p-4">
    <div class="flex items-center justify-between">
      <h1 class="text-2xl font-bold">{{ $pitch->title }}</h1>
      <div class="flex items-center">
        @if ($average_rating <= 0)
          <span class='text-2xl'>No ratings for this pitch yet</span>
        @else
          <span class="me-2 text-2xl">Rated</span>
          <x-star-rating-field :size="7" :rating="$average_rating"></x-star-rating-field>
        @endif
      </div>
    </div>
    <div class="mt-4 text-lg">
      <p class=""><span class="font-semibold">Sport </span>: {{ $pitch->sports->title }}</p>
      <p class="">
        <span class="font-semibold">Dimensions:</span>
        <span>{{ $dimensionValues['outside'] }}</span>
        <span class="text-amber-400">({{ $dimensionValues['inside'] }})</span>
      </p>
      <p class="mt-1">
        <span class="font-semibold">Grass Type:</span>
        <span>{{ $grassTypeValues['outside'] }}</span>
        <span class="text-amber-400">({{ $grassTypeValues['inside'] }})</span>
      </p>
      <p class="mt-1">
        <span class="font-semibold">Max Players:</span>
        <span>{{ $maxPlayers['outside'] }}</span>
        <span class="text-amber-400">({{ $maxPlayers['inside'] }})</span>
      </p>
    </div>

    <div class="flex items-center justify-between text-xl">
      <p class="mt-4 font-semibold">Price for 1 hour : <span
          class="font-bold text-amber-400">{{ $pitch->price_for_hour }}€</span></p>
      <p class="flex items-center gap-1"><svg class="h-8 w-8" xmlns="http://www.w3.org/2000/svg" fill="none"
          viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
          <path stroke-linecap="round" stroke-linejoin="round"
            d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
        </svg>
        {{ $pitch->city->city_name }}, <span class="font-bold">{{ $pitch->state->state_name }}</span></p>
    </div>
    <div class="mt-4 flex items-center justify-between text-xl">
      <p>Booked <span class="font-bold text-amber-400">368</span> times this year </p>
      <p class="flex items-center">Interested? View it / <a
          class="ms-2 bg-white px-5 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
          href="{{ route('pitches.show', ['pitch' => $pitch->slug]) }}" wire:navigate>Book it here!</a></p>
    </div>
  </div>
</div>
