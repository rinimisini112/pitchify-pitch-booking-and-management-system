<div class="border-4 border-white">
  <form class="mx-auto w-4/5 py-8" wire:submit="update">
    <p class="pb-10 text-3xl font-medium text-white">Change your password</p>
    {{ $this->form }}
  </form>
</div>
