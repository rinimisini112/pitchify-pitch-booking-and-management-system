<div class="fixed right-6 top-12 border-4 border-white bg-white bg-opacity-80 text-green-600 shadow-2xl backdrop-blur-sm"
  x-data="{ show: @entangle('show') }" x-show="show" x-transition:enter="transition-transform ease-out duration-200"
  x-transition:enter-start="transform translate-x-full" x-transition:enter-end="transform translate-x-0"
  x-transition:leave="transition-transform ease-in duration-200" x-transition:leave-start="transform translate-x-0"
  x-transition:leave-end="transform translate-x-full" @click="show = false" {{-- Close the notification on click --}}
  @keydown.escape.window="show = false" {{-- Close the notification on ESC key press --}}>
  <div>
    <p class="w-full bg-green-800 py-1.5 pl-6 pr-20 text-xl text-amber-400">Hi {{ auth()->user()->name }}</p>
    <p class="bg-green-600 pb-2 pl-6 pr-20 pt-3 text-white">{{ $message }}</p>
  </div>
</div>
