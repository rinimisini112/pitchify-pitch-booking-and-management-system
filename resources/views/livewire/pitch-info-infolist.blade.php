<div class="mr-auto w-full">

  <div class="sticky top-3 z-[1000] mx-auto w-[90%] border-4 border-white bg-green-800 p-4 shadow-2xl"
    style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')" x-data="{ isOpen: @entangle('showSuccessMessage') }" x-show="isOpen"
    x-transition:enter="transition-transform ease-out duration-300"
    x-transition:enter-start="transform opacity-0 translate-x-[-100%]"
    x-transition:enter-end="transform opacity-100 translate-x-0"
    x-transition:leave="transition-transform ease-in duration-300"
    x-transition:leave-start="transform opacity-100 translate-x-0"
    x-transition:leave-end="transform opacity-0 translate-x-[-100%]" x-on:click.away="isOpen = false">
    <p class="text-2xl font-bold">Hi {{ Auth::user()->name }} thanks for rating us!</p>
    <p class="pt-4">{{ $successMessage }}</p>
  </div>

  <div class="swiper-container relative h-[450px] w-full overflow-clip shadow-inner">
    <div class="absolute right-0.5 top-0 z-50 h-full w-4 shadow-2xl shadow-black">

    </div>
    <div class="swiper-wrapper flex h-full w-full shadow-inner">
      @forelse ($pitch->images as $image)
        <div class="swiper-slide h-full w-full flex-shrink-0 border-4 border-white shadow-2xl">
          <img class="h-full w-full object-cover" src="{{ asset('storage/' . $image) }}" alt="">
        </div>
      @empty
        <div>
          No images available for this pitch yet!
        </div>
      @endforelse
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>

  </div>
  <div class="w-[95%] border-b-8 border-white py-4">
    <p class="text-3xl font-bold">{{ $pitch->title }}</p>
    <div class="mt-3 flex justify-between border-b-4 border-white text-xl">
      <div class="flex items-center">
        @if ($average_rating <= 0)
          <span class='text-2xl'>No ratings for this pitch yet</span>
        @else
          <span class="me-2 text-2xl">Rating</span>
          <x-star-rating-field :size="7" :rating="$average_rating"></x-star-rating-field>
        @endif
      </div>

      <p>Price for 1 hour - <span class="font-bold text-amber-400">{{ $pitch->price_for_hour }}€</span>
        @if ($pitch->pitchWorkingSchedule->is_closed === 1)
          <span
            class="me-0.5 ms-2 bg-white px-4 py-1 font-bold tracking-wide text-red-500 ring-2 ring-white duration-150">
            It's Closed :(</span>
        @else
          <a class="ms-2 inline-block bg-white px-4 py-1 font-bold tracking-wide text-green-700 duration-150 hover:me-0.5 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
            href="{{ route('pitches.reserve', ['pitch' => $pitch->slug]) }}" wire:navigate>Reserve
            Pitch</a>
        @endif
      </p>
    </div>
    <div class="border-b-4 border-white py-3">
      <p class="sports-night-ns pl-2 text-3xl font-bold">Working Days N Schedule</p>
      <div class="flex items-center justify-between text-xl font-medium">
        <p class="pl-3">
          Days -
          @switch($pitch->pitchWorkingSchedule->working_days)
            @case(0)
              <span class="text-amber-400">Every Day</span>
            @break

            @case(1)
              <span class="text-amber-400">Monday to Saturday</span>
            @break

            @case(2)
              <span class="text-amber-400">Monday to Friday</span>
            @break

            @case(3)
              <span class="text-amber-400">Monday to Thursday</span>
            @break

            @case(4)
              <span class="text-amber-400">Monday to Wednesday</span>
            @break

            @default
              N/A
          @endswitch
        </p>
        <p>
          From <span
            class="text-amber-400">{{ \Carbon\Carbon::parse($pitch->pitchWorkingSchedule->works_from)->format('h:i A') }}</span>
          - Until <span
            class="text-amber-400">{{ \Carbon\Carbon::parse($pitch->pitchWorkingSchedule->works_until)->format('h:i A') }}</span>
        </p>

      </div>
    </div>
    <div class="flex w-full items-center justify-between pb-2 pr-4 pt-4">
      <p class="sports-night-ns pl-2 text-3xl font-bold">Pitch Details</p>
      <p class="sports-night-ns text-3xl">Sport - {{ $pitch->sports->title }}</p>
    </div>
    <div class="flex items-center gap-20 border-b-4 border-white pb-4">
      <div class="w-3/5 border-r-4 border-white text-xl">
        <p class="mt-2 font-black">Dimensions : <span
            class="mt-1 block pl-6 text-amber-400">{{ $pitch->dimension }}</span></p>
        <p class="mt-2 font-black">Type : <span class="mt-1 block pl-6 text-amber-400">{{ $pitch->pitch_type }}</span>
        </p>
        <p class="mt-2 font-black">Player Limit : <span
            class="mt-1 block pl-6 text-amber-400">{{ $pitch->player_limit }}</span></p>
      </div>
      <div class="w-2/5 pr-4">
        <p class="text-xl font-black">More from this sport</p>
        <a class="group relative mt-2 inline-block h-[150px] w-full overflow-clip border-4 border-white shadow-inner drop-shadow-xl"
          href="{{ route('pitches.sportsPitches', [$pitch->sports->slug]) }}" wire:navigate>
          <img class="block h-full w-full object-cover duration-200 group-hover:scale-110"
            src="{{ asset('storage/' . $pitch->sports->thumbnail) }}" alt="">
          <li
            class="absolute left-0 top-0 h-full w-full bg-gradient-to-br from-[#166534bc] via-[#16653465] to-transparent p-3 text-xl font-semibold duration-300 group-hover:bg-none">
            {{ $pitch->sports->title }}</li>
        </a>
      </div>
    </div>
    <div class="flex items-center border-b-4 border-white pb-4">
      <div class="w-1/2">
        <p class="sports-night-ns flex items-center gap-2 pl-2 pt-4 text-3xl"><svg class="feather feather-map"
            xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round">
            <polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon>
            <line x1="8" y1="2" x2="8" y2="18"></line>
            <line x1="16" y1="6" x2="16" y2="22"></line>
          </svg> Location</p>
        <div class="text-2xl">
          <p class="pt-6 text-xl">District and Locality </p>
          <p class="pl-12 font-bold text-amber-400">
            {{ $pitch->city->city_name . ', ' . $pitch->state->state_name }}</p>
          <p class="pt-3 text-xl">Address </p>
          <p class="pl-12 font-bold text-amber-400">{{ $pitch->address }}</p>
        </div>
      </div>
      <div class="flex w-1/2 flex-col items-end pr-4">
        @if (auth()->user()->ratingPitches()->where('pitch_id', $pitch->id)->exists())
          <p class="sports-night pr-10 pt-4 text-2xl font-semibold">Thanks for rating</p>
          <div class="flex items-center">
            <p class="flex items-center pt-1">Your oppinion matters to us
            </p>
          </div>
        @else
          <p class="sports-night pr-10 pt-4 text-2xl font-semibold">Enjoyed the Pitch?</p>
          <div class="flex items-center">
            <p class="flex items-center pt-1"> Give it a <svg class="feather feather-star ms-1"
                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="#FDCC0D"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <polygon
                  points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2">
                </polygon>
              </svg>

            </p>
            @livewire('rating-modal', ['pitch' => $pitch])
          </div>
        @endif
      </div>
    </div>
    <div>
      <p class="sports-night-ns pl-2 pt-4 text-3xl">Pitch Description</p>
      <div class="pl-4 pr-8" id="pitch-description-box">
        {!! $pitch->description !!}
      </div>
    </div>
  </div>
  <script></script>
</div>
