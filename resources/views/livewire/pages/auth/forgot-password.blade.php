<?php

use Illuminate\Support\Facades\Password;
use Livewire\Attributes\Layout;
use Livewire\Volt\Component;

new #[Layout('layouts.guest')] class extends Component {
    public string $email = '';

    /**
     * Send a password reset link to the provided email address.
     */
    public function sendPasswordResetLink(): void
    {
        $this->validate([
            'email' => ['required', 'string', 'email'],
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = Password::sendResetLink($this->only('email'));

        if ($status != Password::RESET_LINK_SENT) {
            $this->addError('email', __($status));

            return;
        }

        $this->reset('email');

        session()->flash('status', __($status));
    }
}; ?>

@section('title', 'Forgot your password?')
<div class="relative flex h-screen w-full items-center justify-center overflow-hidden">
  <div class="absolute right-6 top-4 text-2xl font-bold">
    <a class="" href="{{ route('home') }}">Go to Home</a>
    <a class="mt-2 block" href="{{ route('login') }}">Back to Login</a>
  </div>
  <div class="flex h-full w-3/4 items-center">
    <div class="relative z-50 mb-10 mt-4 h-[65%] w-2/5 border-4 border-white bg-green-800 shadow-2xl"
      style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')">
      <div class="mx-auto w-4/5">
        <p class="mt-12 text-center text-3xl font-bold">Forgot your Password?</p>
        <div class="mb-4 mt-2 text-sm text-gray-400 dark:text-gray-300">
          {{ __('No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </div>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <form wire:submit="sendPasswordResetLink">
          <!-- Email Address -->
          <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input class="mt-1 block w-full" id="email" name="email" type="email"
              placeholder='Enter you current email' wire:model="email" required autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('email')" />
          </div>

          <div class="mt-6 flex items-center justify-end">
            <x-primary-button>
              {{ __('Email Password Reset Link') }}
            </x-primary-button>
          </div>
        </form>
      </div>
    </div>
    <div class="relative h-full w-3/5" id="animation-container">
      <img class="relative -left-4 z-10 h-full w-full object-cover" id="register-player"
        src="{{ asset('images/login-register-png-player.png') }}" alt="">
      <img class="absolute -left-3 top-16 z-0 h-3/4 w-full" id="register-background"
        src="{{ asset('images/login-register-png-background.png') }}" alt="">
    </div>
  </div>
</div>
