<?php

use App\Livewire\Forms\LoginForm;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\Layout;
use Livewire\Volt\Component;

new #[Layout('layouts.guest')] class extends Component {
    public LoginForm $form;

    /**
     * Handle an incoming authentication request.
     */
    public function login(): void
    {
        $this->validate();

        $this->form->authenticate();

        Session::regenerate();

        $this->redirect(session('url.intended', RouteServiceProvider::HOME), navigate: true);
    }
}; ?>

@section('title', 'Login on your account on Pitchify')
<div class="relative flex h-screen w-full items-center justify-center overflow-hidden">
  <div class="absolute right-6 top-4 text-2xl font-bold">
    <a class="" href="{{ route('home') }}" wire:navigate>Back to Home</a>
    <a class="mt-2 block" href="{{ route('register') }}" wire:navigate>Go to Register</a>
  </div>
  <div class="flex h-full w-3/4 items-center">
    <div class="relative z-50 mb-[4.5rem] mt-10 h-4/5 w-2/5 border-4 border-white bg-green-800 shadow-2xl"
      style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')">
      <!-- Session Status -->
      <x-auth-session-status class="mb-4" :status="session('status')" />
      <form class='mx-auto w-4/5' wire:submit="login">
        <p class="my-8 text-center text-3xl font-bold">Login on your account</p>
        <!-- Email Address -->
        <div>
          <x-input-label for="email" :value="__('Email')" />
          <x-text-input class="mt-1 block w-full" id="email" name="email" type="email"
            placeholder='Enter your e-mail address' wire:model="form.email" required autocomplete="username" />
          <x-input-error class="mt-2" :messages="$errors->get('email')" />
        </div>

        <!-- Password -->
        <div class="mt-4">
          <x-input-label for="password" :value="__('Password')" />

          <x-text-input class="mt-1 block w-full" id="password" name="password" type="password"
            placeholder='Enter your password' wire:model="form.password" required autocomplete="current-password" />

          <x-input-error class="mt-2" :messages="$errors->get('password')" />
        </div>

        <!-- Remember Me -->
        <div class="mt-4 block">
          <label class="inline-flex items-center" for="remember">
            <input class="rounded border-white text-amber-400 shadow-sm focus:ring-amber-500" id="remember"
              name="remember" type="checkbox" wire:model="form.remember">
            <span class="ms-2 text-base text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
          </label>
        </div>

        <div class="mt-4 flex items-center justify-end">
          @if (Route::has('password.request'))
            <a class="rounded-md text-sm text-gray-600 underline hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:text-gray-400 dark:hover:text-gray-100 dark:focus:ring-offset-gray-800"
              href="{{ route('password.request') }}">
              {{ __('Forgot your password?') }}
            </a>
          @endif

          <x-primary-button class="ms-3">
            {{ __('Log in') }}
          </x-primary-button>
        </div>

        <p class="mt-16 text-center text-sm"><a class="text-3xl font-bold" href="">Pitchify</a> <span
            class="ms-2">Your
            virtual sport
            assistant</span></p>
      </form>
    </div>
    <div class="relative h-full w-3/5" id="animation-container">
      <img class="relative -left-8 -top-6 z-10 w-full object-cover" id="register-player"
        src="{{ asset('images/login-png-player.png') }}" alt="">
      <img class="absolute -left-3 top-0 z-0 h-full w-full" id="register-background"
        src="{{ asset('images/login-register-png-background.png') }}" alt="">
    </div>
  </div>
</div>
