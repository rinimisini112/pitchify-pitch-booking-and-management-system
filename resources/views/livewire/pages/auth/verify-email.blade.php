<?php

use App\Livewire\Actions\Logout;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\Layout;
use Livewire\Volt\Component;

new #[Layout('layouts.app')] class extends Component {
    /**
     * Send an email verification notification to the user.
     */
    public function sendVerification(): void
    {
        if (Auth::user()->hasVerifiedEmail()) {
            $this->redirect(session('url.intended', RouteServiceProvider::HOME), navigate: true);

            return;
        }

        Auth::user()->sendEmailVerificationNotification();

        Session::flash('status', 'verification-link-sent');
    }

    /**
     * Log the current user out of the application.
     */
    public function logout(Logout $logout): void
    {
        $logout();

        $this->redirect('/', navigate: true);
    }
}; ?>
@section('title', 'Verify your Pitchify account email')
<div class="flex h-[85vh] w-full items-center justify-center overflow-hidden">
  <div class="w-2/5 border-4 border-white px-20 py-8">
    <p class="border-b-4 border-white py-1 text-center text-base text-white">Navigate Back - </p>
    <div class="flex items-center justify-center gap-6 border-b-4 border-b-white py-1 text-lg text-white">
      <a class="duration-200 ease-in-out hover:text-gray-300" href="{{ route('profile') }}">Back to Profile</a>
      <a class="duration-200 ease-in-out hover:text-gray-300" href="{{ route('dashboard') }}">Back to Dashboard</a>
    </div>
    <div class="my-4 text-sm text-gray-300">
      <p class="pb-6 pt-1 text-center text-2xl font-bold text-white">Verify your E-mail</p>
      {{ __('Thanks for taking your time to verify! Before getting started booking pitches we need to make sure that you are a real person, you can verify your email address by clicking on the link below? If you didn\'t receive the email, we will gladly send you another.') }}
    </div>

    @if (session('status') == 'verification-link-sent')
      <div class="mb-4 text-sm font-medium text-green-600 dark:text-green-400">
        {{ __('A new verification link has been sent to the email address you provided during registration.') }}
      </div>
    @endif

    <div class="mt-6 flex items-center justify-center">
      <x-primary-button wire:click="sendVerification">
        {{ __('Send Verification Email') }}
      </x-primary-button>

    </div>
  </div>
</div>
