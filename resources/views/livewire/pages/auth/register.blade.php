<?php

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Livewire\Attributes\Layout;
use Livewire\Volt\Component;

new #[Layout('layouts.guest')] class extends Component {
    public string $name = '';
    public string $email = '';
    public string $password = '';
    public string $password_confirmation = '';

    /**
     * Handle an incoming registration request.
     */
    public function register(): void
    {
        $validated = $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'string', 'confirmed', Rules\Password::defaults()],
        ]);

        $validated['password'] = Hash::make($validated['password']);

        event(new Registered(($user = User::create($validated))));

        Auth::login($user);

        $this->redirect(RouteServiceProvider::HOME, navigate: true);
    }
}; ?>
@section('title', 'Register on Pitchify')
<div class="relative flex h-screen w-full items-center justify-center overflow-hidden">
  <div class="absolute right-6 top-4 text-2xl font-bold">
    <a class="duration-150 hover:text-gray-300" href="{{ route('home') }}" wire:navigate>Back to Home</a>
    <a class="mt-2 block duration-150 hover:text-gray-300" href="{{ route('login') }}" wire:navigate>Go to Login</a>
  </div>
  <div class="flex h-full w-3/4 items-center">
    <div class="relative z-50 mb-3 mt-10 h-[90%] w-2/5 border-4 border-white bg-green-800 shadow-2xl"
      style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')">
      <form class="mx-auto w-4/5" wire:submit="register">
        <!-- Name -->
        <p class="my-5 text-center text-3xl font-semibold">Register your account on Pitchify</p>
        <div>
          <x-input-label for="name" :value="__('Name')" />
          <x-text-input class="mt-1 block w-full" id="name" name="name" type="text" wire:model="name"
            required autofocus placeholder='Example John Doe' autocomplete="name" />
          <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>

        <!-- Email Address -->
        <div class="mt-4">
          <x-input-label for="email" :value="__('Email')" />
          <x-text-input class="mt-1 block w-full" id="email" name="email" type="email" wire:model="email"
            required placeholder='Example john@example.com' autocomplete="username" />
          <x-input-error class="mt-2" :messages="$errors->get('email')" />
        </div>

        <!-- Password -->
        <div class="mt-4">
          <x-input-label for="password" :value="__('Password')" />

          <x-text-input class="mt-1 block w-full" id="password" name="password" type="password" wire:model="password"
            required placeholder='This part is up to you' autocomplete="new-password" />

          <x-input-error class="mt-2" :messages="$errors->get('password')" />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
          <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

          <x-text-input class="mt-1 block w-full" id="password_confirmation" name="password_confirmation"
            type="password" wire:model="password_confirmation" required placeholder='Dont forget to confirm it'
            autocomplete="new-password" />

          <x-input-error class="mt-2" :messages="$errors->get('password_confirmation')" />
        </div>

        <div class="mt-4 flex items-center justify-end">
          <a class="rounded-md text-sm text-gray-300 duration-150 hover:text-white active:outline-amber-400 active:ring-amber-400"
            href="{{ route('login') }}" wire:navigate>
            {{ __('Already have an account?') }}
          </a>

          <x-primary-button class="ms-4">
            {{ __('Register') }}
          </x-primary-button>
        </div>
      </form>
    </div>
    <div class="relative h-full w-3/5" id="animation-container">
      <img class="relative -left-4 z-10 h-full w-full object-cover" id="register-player"
        src="{{ asset('images/login-register-png-player.png') }}" alt="">
      <img class="absolute -left-3 top-0 z-0 h-full w-full" id="register-background"
        src="{{ asset('images/login-register-png-background.png') }}" alt="">
    </div>
  </div>
</div>
