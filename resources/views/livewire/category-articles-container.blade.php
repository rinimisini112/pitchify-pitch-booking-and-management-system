<div class="w-full pt-6">
  @forelse ($posts as $post)
    <x-news-card-small :articleLink="route('news.article.show', ['post' => $post->slug])" :imageUrl="asset('storage/' . $post->thumbnail)" :newsTitle="$post->title" :newsTimestamp="$post->published_at" />
  @empty
    <p class="py-10 text-center text-2xl font-bold">No news articles available for this category yet!</p>
  @endforelse
  <div class="w-[90%]">
    {{ $posts->links() }}
  </div>
</div>
