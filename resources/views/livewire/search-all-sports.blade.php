<div>
  <div class="mx-auto mt-8 w-4/5 border-8 border-white">
    <div class="mx-auto w-4/5">
      <span class="me-2 py-4 text-center text-lg font-semibold">Search Sports</span>
      <x-text-input class="my-4 w-4/5 border-2 bg-white" type="text" wire:model.live.debounce.300ms="query"
        placeholder="Search..."></x-text-input>
    </div>
  </div>
  <div class="mx-auto mb-4 mt-8 w-4/5 border-x-8 border-t-8 border-white">
    <div class="mx-auto flex w-[95%] flex-wrap items-center justify-center gap-8 pb-24 pt-6 duration-75"
      wire:loading.class="opacity-0 -translate-x-2">
      @forelse ($results as $sport_result)
        <a class="relative mt-6 inline-block w-2/5 overflow-clip rounded-lg border-4 border-white duration-150 ease-in hover:ring hover:ring-white"
          href=''>
          <img class="h-[240px] w-full object-cover" src="{{ asset('storage/' . $sport_result->thumbnail) }}"
            alt="">
          <div
            class="absolute left-0 top-0 flex h-full w-full flex-col justify-between bg-gradient-to-t from-[#2e7d32a0] via-transparent to-green-800 px-3 py-1 text-left duration-150 hover:opacity-0">
            <p class="text-xl font-bold">{{ $sport_result->title }}</p>
            <p class="place-self-end font-medium">34 Pitches for this sport</p>
          </div>
        </a>
      @empty
        <p class="py-12 text-center text-xl font-medium">No Sports found matching your search!</p>
      @endforelse
    </div>
  </div>
</div>
