<div class="z-10 p-6 text-end text-lg sm:fixed sm:right-0 sm:top-0">
  @auth
    <a class="font-semibold text-white duration-100 hover:text-gray-300 active:scale-90 active:text-gray-600"
      href="{{ url('/dashboard') }}" wire:navigate>Dashboard</a>
  @else
    <a class="ms-6 font-semibold text-white duration-100 hover:text-gray-300 active:scale-90 active:text-gray-600"
      href="{{ route('login') }}" wire:navigate>Our Pitches</a>
    <a class="ms-6 font-semibold text-white duration-100 hover:text-gray-300 active:scale-90 active:text-gray-600"
      href="{{ route('login') }}" wire:navigate>Log in</a>
    @if (Route::has('register'))
      <a class="ms-6 font-semibold text-white duration-100 hover:text-gray-300 active:scale-90 active:text-gray-600"
        href="{{ route('register') }}" wire:navigate>Register</a>
    @endif
  @endauth
</div>
