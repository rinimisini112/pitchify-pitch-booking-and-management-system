<div class="border-4 border-white">
  <form class="mx-auto w-4/5 py-8" wire:submit="update">
    @livewire('notification-popup')
    <p class="pb-10 text-3xl font-medium text-white">Update your profile credentials</p>
    {{ $this->form }}

    <button
      class="mt-4 bg-white px-12 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8 md:-mt-8"
      type="submit">
      Update Profile
    </button>
  </form>
  <div class="mx-auto w-4/5 border-4 border-white pb-4 text-center">
    <p class="pb-6 pt-4 text-2xl font-bold text-white">Email verification status :</p>
    @if (Auth::user()->email_verified_at)
      <p class="flex items-center justify-center gap-1 font-semibold text-white">Email <span
          class="flex items-center gap-1 text-amber-400">Verified <svg class="feather feather-check"
            xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <polyline points="20 6 9 17 4 12"></polyline>
          </svg></span></p>
    @else
      <p class="font-semibold text-white">Email <span class="bg-white px-1 py-0.5 text-red-600">Not Verified</span> yet!
        <a class="ms-3 bg-white px-4 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
          href="{{ route('verification.notice') }}">Click here
          to verify</a>
      </p>
    @endif
  </div>

  <x-filament-actions::modals />
</div>
