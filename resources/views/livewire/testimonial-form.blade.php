<form class="w-3/5 border-8 border-white p-6" wire:submit.prevent="saveTestimonial">

  <div class="flex w-full items-center">
    @if (auth()->user()->profile_picture)
      <img class="aspect-square w-[15%] flex-shrink-0 rounded-full object-cover ring ring-amber-400"
        src="{{ 'storage/' . auth()->user()->profile_picture }}" alt="">
    @else
      <div
        class="flex aspect-square w-11 flex-shrink-0 items-center justify-center rounded-full bg-white text-2xl font-light text-green-800 ring ring-amber-400 focus:ring-green-800 active:scale-90 active:ring-8">
        R</div>
    @endif
    <p class="pl-4 text-xl font-bold">Hi {{ Auth::user()->name }}!! <span class="block text-base font-light">Are you
        satisfied with our app? If
        you are
        be kind an leave us
        a
        review of our app,
        we take our customers very seriously and we are open to hearing your feedback.</span></p>
  </div>
  <div class="mt-6 border-4 border-white p-4">
    <label for="content">
      Write your review

      <textarea
        class="min-h-48 mt-2 w-full min-w-full max-w-full border-2 border-white bg-white bg-opacity-50 text-green-800 outline-none duration-100 focus:border-green-800 focus:bg-opacity-100 focus:ring focus:ring-amber-400"
        id="content" name="content" wire:model="content"></textarea>

    </label>
    <div class="flex w-full items-center justify-end gap-4">
      @error('content')
        <span class="bg-opacity-65 mt-2 bg-white px-4 py-1 font-bold text-red-500">{{ $message }}</span>
      @enderror
      <button
        class="mt-2 bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
        type="submit">
        Submit Review
      </button>
    </div>
  </div>
</form>
