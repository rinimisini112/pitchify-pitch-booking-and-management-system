<?php

use App\Livewire\Actions\Logout;
use Livewire\Volt\Component;

new class extends Component {
    /**
     * Log the current user out of the application.
     */
    public function logout(Logout $logout): void
    {
        $logout();

        $this->redirect('/', true);
    }
}; ?>

<nav class="sticky top-0 z-50 bg-green-800 text-white"
  style="background-image: url('{{ asset('images/football-no-lines.png') }}')" x-data="{ open: false }">
  <!-- Primary Navigation Menu -->
  <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-6">
    <div class="flex h-16 justify-between">
      <div class="flex">
        <!-- Logo -->
        <div class="flex shrink-0 items-center">
          <p class="text-3xl font-bold"><a href="{{ route('dashboard') }}" wire:navigate>
              Pitchify
            </a>
          </p>

        </div>
      </div>
      <!-- Navigation Links -->
      <div class="flex">
        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
          <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" wire:navigate>
            {{ __('Dashboard') }}
          </x-nav-link>
        </div>
        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
          <x-nav-link :href="route('pitches.index')" :active="request()->routeIs([
              'pitches.index',
              'pitches.show',
              'pitches.allSports',
              'pitches.sportsPitches',
              'pitches.reserve',
          ])" wire:navigate>
            {{ __('Pitches') }}
          </x-nav-link>
        </div>
        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
          <x-nav-link :href="route('news-articles.index')" :active="request()->routeIs([
              'news-articles.index',
              'news-articles.all',
              'news.article.show',
              'news-articles.categories',
          ])" wire:navigate>
            {{ __('News') }}
          </x-nav-link>
        </div>
        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
          <x-nav-link :href="route('dashboard')" wire:navigate>
            {{ __('My Reservations') }}
          </x-nav-link>
        </div>
      </div>

      <!-- Settings Dropdown -->
      <div class="hidden sm:ms-6 sm:flex sm:items-center">
        <x-dropdown align="right" width="48">
          <x-slot name="trigger">
            <button
              class="inline-flex items-center rounded-md border border-transparent px-3 py-2 text-lg font-medium leading-4 text-white transition duration-150 ease-in-out hover:text-gray-300 focus:outline-none">
              @if (auth()->user()->profile_picture)
                <img
                  class="aspect-square w-11 rounded-full object-cover ring ring-amber-400 focus:ring-green-800 active:scale-90 active:ring-8"
                  src="{{ asset('storage/' . auth()->user()->profile_picture) }}" alt="">
              @else
                @php
                  $firstName = explode(' ', Auth::user()->name)[0];
                  $firstLetter = strtoupper(substr($firstName, 0, 1));
                @endphp
                <div
                  class="flex aspect-square w-11 items-center justify-center rounded-full bg-white text-2xl font-thin text-green-800 ring ring-amber-400 focus:ring-green-800 active:scale-90 active:ring-8">
                  {{ $firstLetter }}</div>
              @endif
            </button>
          </x-slot>

          <x-slot name="content">
            <x-dropdown-link :href="route('profile')">
              {{ __('Profile') }}
            </x-dropdown-link>

            <!-- Authentication -->
            <button class="w-full text-start" wire:click="logout">
              <x-dropdown-link>
                {{ __('Log Out') }}
              </x-dropdown-link>
            </button>
          </x-slot>
        </x-dropdown>
      </div>

      <!-- Hamburger -->
      <div class="-me-2 flex items-center sm:hidden">
        <button
          class="inline-flex items-center justify-center rounded-md p-2 text-gray-400 transition duration-150 ease-in-out hover:bg-gray-100 hover:text-gray-500 focus:bg-gray-100 focus:text-gray-500 focus:outline-none dark:text-gray-500 dark:hover:bg-gray-900 dark:hover:text-gray-400 dark:focus:bg-gray-900 dark:focus:text-gray-400"
          @click="open = ! open">
          <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
            <path class="inline-flex" :class="{ 'hidden': open, 'inline-flex': !open }" stroke-linecap="round"
              stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
            <path class="hidden" :class="{ 'hidden': !open, 'inline-flex': open }" stroke-linecap="round"
              stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
          </svg>
        </button>
      </div>
    </div>
  </div>

  <!-- Responsive Navigation Menu -->
  <div class="hidden sm:hidden" :class="{ 'block': open, 'hidden': !open }">
    <div class="space-y-1 pb-3 pt-2">
      <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" wire:navigate>
        {{ __('Dashboard') }}
      </x-responsive-nav-link>
    </div>

    <!-- Responsive Settings Options -->
    <div class="border-t border-gray-200 pb-1 pt-4 dark:border-gray-600">
      <div class="px-4">
        <div class="text-base font-medium text-gray-800 dark:text-gray-200" x-data="{{ json_encode(['name' => auth()->user()->name]) }}" x-text="name"
          x-on:profile-updated.window="name = $event.detail.name"></div>
        <div class="text-sm font-medium text-gray-500">{{ auth()->user()->email }}</div>
      </div>

      <div class="mt-3 space-y-1">
        <x-responsive-nav-link :href="route('profile')" wire:navigate>
          {{ __('Profile') }}
        </x-responsive-nav-link>

        <!-- Authentication -->
        <button class="w-full text-start" wire:click="logout">
          <x-responsive-nav-link>
            {{ __('Log Out') }}
          </x-responsive-nav-link>
        </button>
      </div>
    </div>
  </div>
</nav>
