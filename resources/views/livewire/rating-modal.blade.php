<div class="inline">
  <button
    class="ms-2 inline-block border-4 border-white px-4 py-0.5 text-lg font-bold uppercase tracking-wide text-white duration-150 hover:bg-white hover:text-green-800 hover:ring hover:ring-white active:ring-8"
    x-data="" x-on:click.prevent="$dispatch('open-modal', 'rating-form')">Here!</button>
  <x-modal name="rating-form" :show="$errors->isNotEmpty()" focusable>
    <form class="p-6" wire:submit.prevent="submitRating">
      <div class="flex items-center gap-2">
        @if (auth()->user()->profile_picture)
          <img class="aspect-square w-14 flex-shrink-0 rounded-full object-cover ring ring-amber-400"
            src="{{ asset('storage/' . Auth::user()->profile_picture) }}" alt="">
        @else
          @php
            $firstName = explode(' ', Auth::user()->name)[0];
            $firstLetter = strtoupper(substr($firstName, 0, 1));
          @endphp
          <div
            class="flex aspect-square w-14 flex-shrink-0 items-center justify-center rounded-full bg-white text-2xl font-light text-green-800 ring ring-amber-400 focus:ring-green-800 active:scale-90 active:ring-8">
            {{ $firstLetter }}</div>
        @endif
        <h2 class="text-xl font-medium text-gray-900 dark:text-gray-100">
          {{ __('Hi ' . Auth::user()->name . '!') }}
        </h2>
      </div>

      <p class="mt-1 dark:text-gray-300">
        {{ __('If you were satisfied with ' . $pitch->title . ' services and quality, you can give it a rating and a comment to support our local businesess.') }}
      </p>
      <div class="mt-6">
        <p>Rate the pitch</p>
        <label class="rating-label" class="bg-transparent">
          <input class="rating bg-transparent" type="range" value="0" style="--value:0" wire:model="rating"
            max="5" oninput="this.style.setProperty('--value', `${this.valueAsNumber}`)" step="1">
        </label>
        <x-input-error class="mt-2" :messages="$errors->get('rating')" />
      </div>
      <div class="mt-3">
        <label for="comment">
          <span class="block">Leave a comment if you want</span>
          <textarea
            class="min-h-28 mt-1 max-h-28 min-w-full max-w-full border-2 border-amber-400 text-green-800 outline-none duration-150 ease-in-out focus:border-green-800 focus:ring focus:ring-amber-400"
            id="comment" name="comment" wire:model="comment"></textarea>
          <x-input-error class="mt-2" :messages="$errors->get('comment')" />
        </label>
      </div>

      <div class="mt-6 flex justify-end">
        <x-secondary-button x-on:click="$dispatch('close')">
          {{ __('Cancel') }}
        </x-secondary-button>

        <x-primary-button class="ms-3" type="submit">
          {{ __('Submit Rating') }}
        </x-primary-button>
      </div>
    </form>
  </x-modal>
</div>
