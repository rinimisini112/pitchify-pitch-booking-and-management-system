@if (!$posts)
  <div>

  </div>
@else
  <div class="mx-auto flex w-fit flex-col items-center justify-center">
    <p class="py-2 text-2xl font-bold">More from this category</p>
    @forelse ($posts as $post)
      <a class="relative ml-auto mt-4 inline-block w-[90%] border-4 border-white duration-150 hover:ring hover:ring-white"
        href="{{ route('news.article.show', ['post' => $post->slug]) }}" wire:navigate>
        <img class="block h-[220px] max-h-[220px] w-full object-cover" src="{{ asset('storage/' . $post->thumbnail) }}"
          alt="">
        <div
          class="absolute left-0 top-0 flex h-full w-full flex-col justify-between bg-gradient-to-t from-[#2e7d32c5] via-transparent to-green-800 p-0.5 pl-1 text-left duration-150 hover:opacity-0">
          @if (Str::length($post->title) > 60)
            <p class="text-xl font-bold">{{ $post->title = Str::limit($post->title, 57, '...') }}</p>
          @else
            <p class="text-xl font-bold">{{ $post->title }}</p>
          @endif
          <p>{{ $post->formatted_published_at }}</p>
        </div>
      </a>
    @empty
      <div></div>
    @endforelse
  </div>
@endif
