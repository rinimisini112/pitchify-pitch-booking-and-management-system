@if ($this->allHeadingsSet())
  <div class="flex h-[530px] w-full border-4 border-white">
    <a class="group relative inline-block h-full w-1/2 border-r-4 border-white"
      href="{{ route('news.article.show', ['post' => $mainPost->slug]) }}">
      <div
        class="absolute left-0 top-0 h-full w-full bg-green-700 bg-opacity-40 p-3 duration-200 group-hover:bg-opacity-0">
        <p class="text-xl font-medium">{{ $mainPost->title }}</p>
      </div>
      <img class="h-full w-full object-cover" src="{{ asset('storage/' . $mainPost->thumbnail) }}" alt="The main heading">
    </a>
    <div class="flex h-full w-1/2 flex-col">
      <a class="group relative inline-block h-1/2 w-full overflow-clip border-b-4 border-white"
        href="{{ route('news.article.show', ['post' => $mediumPost->slug]) }}">
        <div
          class="absolute left-0 top-0 h-full w-full bg-green-700 bg-opacity-40 p-3 duration-200 group-hover:bg-opacity-0">
          <p class="text-xl font-medium">{{ $mediumPost->title }}</p>
        </div>
        <img class="h-full w-full object-cover" src="{{ asset('storage/' . $mediumPost->thumbnail) }}" alt="">
      </a>
      <div class="flex h-1/2 w-full">
        <a class="group relative inline-block h-full w-1/2 border-r-4 border-white"
          href="{{ route('news.article.show', ['post' => $smallPost1->slug]) }}">
          <div
            class="absolute left-0 top-0 h-full w-full bg-green-700 bg-opacity-40 p-3 duration-200 group-hover:bg-opacity-0">
            <p class="text-xl font-medium">{{ $smallPost1->title }}</p>
          </div>
          <img class="h-full w-full object-cover" src="{{ asset('storage/' . $smallPost1->thumbnail) }}" alt="">
        </a>
        <a class="group relative inline-block h-full w-1/2"
          href="{{ route('news.article.show', ['post' => $smallPost2->slug]) }}">
          <div
            class="absolute left-0 top-0 h-full w-full bg-green-700 bg-opacity-40 p-3 duration-200 group-hover:bg-opacity-0">
            <p class="text-xl font-medium">{{ $smallPost2->title }}</p>
          </div>
          <img class="h-full w-full object-cover" src="{{ asset('storage/' . $smallPost2->thumbnail) }}"
            alt="">
        </a>
      </div>
    </div>
  </div>
@endif
