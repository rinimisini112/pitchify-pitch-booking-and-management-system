<div class="w-full pt-6">
  @foreach ($posts as $post)
    <x-news-card-small :articleLink="route('news.article.show', ['post' => $post->slug])" :imageUrl="asset('storage/' . $post->thumbnail)" :newsTitle="$post->title" :newsTimestamp="$post->published_at" />
  @endforeach
  <div class="w-[90%]">
    {{ $posts->links() }}
  </div>
</div>
