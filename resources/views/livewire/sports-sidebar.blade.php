<div class="w-full py-4">
  <h1 class="text-center text-3xl font-semibold">Choose your sport</h1>
  <p class="pt-2 text-center text-xl text-amber-400">We have them all</p>
  <ul class="mx-auto w-3/4">

    @foreach ($sports as $sport)
      <a class="group relative mt-2 inline-block h-[130px] w-full overflow-clip rounded-2xl shadow-inner drop-shadow-xl"
        href="{{ route('pitches.sportsPitches', [$sport->slug]) }}" wire:navigate>
        <img class="block h-full w-full object-cover duration-200 group-hover:scale-110"
          src="{{ asset('storage/' . $sport->thumbnail) }}" alt="">
        <li
          class="absolute left-0 top-0 h-full w-full bg-gradient-to-br from-[#166534bc] via-[#16653465] to-transparent p-3 text-xl font-semibold duration-300 group-hover:bg-none">
          {{ $sport->title }}</li>
      </a>
    @endforeach
    <div class="mt-6 text-center text-xl">
      <p class="text-2xl">There's more!</p>
      <a class="mt-2 inline-block bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
        href="{{ route('pitches.allSports') }}" wire:navigate>View all listed Sports</a>
  </ul>
</div>
