<div class="swiper-slide h-full w-[35%] flex-shrink-0 border-4 border-white shadow-2xl">
  <div class="relative h-1/2 w-full overflow-clip border-b-4 border-white">
    <img class="h-full w-full object-cover" src="{{ asset('storage/' . $first_image) }}" alt="">
    @if ($second_image)
      <img class="absolute left-0 top-0 h-full w-full object-cover opacity-0 duration-150 ease-in hover:opacity-100"
        src="{{ asset('storage/' . $second_image) }}" alt="">
    @endif
  </div>

  <div class="flex h-1/2 flex-col justify-between px-3 pt-3 text-lg">
    <p class="text-2xl font-bold">{{ $pitch->title }}</p>
    <div>
      <p class="mt-2 text-lg">Sport: {{ $pitch->sports->title }}(<span
          class="font-bold text-amber-400">{{ $pitch->sports->title }}</span>)</p>
      <div class="mt-2 text-lg">
        <p class="font-semibold">Price for 1 hour : <span
            class="font-bold text-amber-400">{{ $pitch->price_for_hour }}€</span>
        </p>
        <p class="flex items-center gap-1"><svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
            viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
            <path stroke-linecap="round" stroke-linejoin="round"
              d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
          </svg>
          {{ $pitch->city->city_name }}, <span class="font-bold">{{ $pitch->state->state_name }}</span></p>
      </div>
    </div>
    <div class="flex items-center justify-between pb-1">
      @if ($average_rating <= 0)
        <span class='text-2xl'>No ratings for this pitch yet</span>
      @else
        <x-star-rating-field :size="7" :rating="$average_rating"></x-star-rating-field>
      @endif
      <a class="inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
        href="{{ route('pitches.show', ['pitch' => $pitch->slug]) }}" wire:navigate>Book Now</a>
    </div>
  </div>
</div>
