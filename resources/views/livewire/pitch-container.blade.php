<div class="w-full px-10 py-4" wire:loading.class="opacity-0 -translate-x-2">
  <div class="flex w-[90%] items-center justify-between border-8 border-white py-4 pl-4 pr-8">
    <input
      class="w-3/5 border-amber-400 bg-white bg-opacity-80 text-green-800 shadow-sm shadow-amber-400 outline-none ring-1 duration-75 ease-out focus:border-amber-500 focus:bg-white focus:shadow-xl focus:ring focus:ring-amber-400"
      name="search" type="text" wire:model.live.debounce.400ms="search" placeholder="Search Pitches...">
    <div class="flex items-center gap-2">
      @if ($toggleOpen)
        <p class="sports-night-ns font-bold text-amber-400">Show Open</p>
      @else
        <p class="sports-night-ns font-bold text-amber-400">Show All</p>
      @endif
      <div class="toggle">
        <input id="btn" type="checkbox" wire:model.live.debounce.200ms="toggleOpen">
        <label for="btn">
          <span class="thumb"
            style="background-image: url('{{ asset('images/3D-soccer-ball-on-transparent-background-PNG.png') }}')"></span>
        </label>
        <div class="light"></div>
      </div>
    </div>
  </div>
  @if (!empty($search))
    <h1 class="mt-4 text-3xl font-semibold">Showing results for '{{ $search }}'</h1>
  @else
    <h1 class="mt-4 text-3xl font-semibold">All Pitches</h1>
  @endif
  <div class="py-6 text-center text-xl font-medium" wire:loading wire:target="search">
    <p>Loading Pitches...</p>
  </div>
  <div class="duration-75" wire:loading.class="opacity-0 -translate-x-2">
    @forelse ($pitches as $pitch)
      @livewire('pitch-card-big', [$pitch], key('pitch-' . $pitch->id))
    @empty
      <div class="py-6 text-center text-xl font-medium">No pitches found!</div>
    @endforelse
    <div class="mt-8 w-[90%]">
      {{ $pitches->links() }}
    </div>
  </div>
</div>
