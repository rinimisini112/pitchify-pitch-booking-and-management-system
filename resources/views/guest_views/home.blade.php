@section('title', 'Pitchify Pitch Booking management')
@section('swiper-css')
  <link href="{{ asset('swiper/swiper-bundle.css') }}" rel="stylesheet">
@endsection
<x-guest-layout>
  <x-guest-navigation></x-guest-navigation>
  <div class="w-full" id="layout-container">
    <div class="mx-auto flex w-[95%]">
      <div class="w-[70%] py-8">
        <div class="mr-auto w-full border-b-8 border-white pb-10 pr-10">
          <h1 class="px-10 text-2xl font-medium">
            <span class="text-pretty font-bold text-amber-400">Pitchify</span> your new way of booking a football
            pitches, Basketball
            courts,
            Tennis
            courts, and even
            Ping Pong tables.
          </h1>
          <p class="w-3/4 px-10 pt-4">We make reserving your favourite field <span
              class="text-base font-semibold uppercase text-amber-400">easy</span>, no
            more waiting on
            calls to check if
            your pitch is
            free! check availability and reserve straight from our app!</p>
          <div class="mt-6 px-10">
            <p class="text-3xl font-semibold">A straight to the point person?</p>
            <p class="mt-6 text-xl">View Available<a
                class="ms-4 bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="">
                Pitches
                Now</a></p>
          </div>
        </div>
        <div class="w-full px-10 py-4">
          <h1 class="sports-night text-5xl font-semibold">Most Booked at the moment</h1>
          <div class="mt-6 w-[90%] border-4 border-white">
            <div class="group relative h-[400px] w-full border-b-4 border-white">
              <img class="h-full w-full object-cover" src="{{ asset('images/most-booked-soccer.webp') }}"
                alt="">
              <img
                class="absolute left-0 top-0 h-full w-full object-cover opacity-0 duration-200 ease-in group-hover:opacity-100"
                src="{{ asset('images/most-booked-soccer-fade.webp') }}" alt="">
            </div>
            <div class="p-4">
              <div class="flex items-center justify-between">
                <h1 class="text-2xl font-bold">Olympic Stadium Llapnasella</h1>
                <div class="flex items-center">
                  <span class="me-2 text-2xl">Rated</span>
                  <x-star-rating-field :rating="4.25"></x-star-rating-field>
                </div>
              </div>
              <div class="mt-4 text-lg">
                <p class=""><span class="font-semibold">Sport </span>: Football(<span
                    class="text-amber-400">Soccer</span>)</p>
                <p class=""><span class="font-semibold">Dimensions </span>: Standard Proffesional(<span
                    class="text-amber-400">105 x 67m</span>)</p>
                <p class="mt-1"><span class="font-semibold">Grass Type </span>: Artificial(<span
                    class="text-amber-400">Turf Grass</span>)</p>
                <p class="mt-1"><span class="font-semibold">Max Players </span>: Standard 11<span
                    class="text-amber-400">v</span>11(<span class="text-amber-400">Total 22 Players</span>)</p>
              </div>
              <div class="flex items-center justify-between text-xl">
                <p class="mt-4 font-semibold">Price for 1 hour : <span class="font-bold text-amber-400">60€</span></p>
                <p class="flex items-center gap-1"><svg class="h-8 w-8" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                    <path stroke-linecap="round" stroke-linejoin="round"
                      d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
                  </svg>
                  Llapnsasella, <span class="font-bold">Veterrnik</span></p>
              </div>
              <div class="mt-4 flex items-center justify-between text-2xl">
                <p>Booked <span class="font-bold text-amber-400">368</span> times this year </p>
                <p class="flex items-center">Interested? <a
                    class="ms-2 bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                    href="">Book it here!</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="w-[30%] border-l-8 border-white py-4 text-white">
        @livewire('sports-sidebar', [$sports])
      </div>
    </div>
    <div class="w-full border-y-8 border-white p-4">
      <div class="mx-auto w-4/5 p-4">
        <p class="text-center text-3xl"><span class="bg-white px-2 font-semibold text-green-800">Pitchify
            News!</span>
          Stay
          updated with everything
          sport related in Kosovo</p>
        <h3 class="sports-night-ns mt-8 text-4xl tracking-wider">Featured</h3>
        @livewire('news-headings-widget')
        <div class="mt-8 flex w-full gap-4">
          <div class="w-[65%] pr-6">
            <div class="w-full border-b-4 border-white">
              <span class="bg-white px-4 py-0.5 text-2xl font-semibold text-green-800">Latest news</span>
            </div>
            <div class="w-full pt-6">
              @foreach ($posts as $post)
                @php
                  $publishedAt = \Carbon\Carbon::parse($post->published_at);
                  $formattedTimestamp = $publishedAt->format('F jS Y h:i A');
                @endphp
                <x-news-card-small :articleLink="route('news.article.show', ['post' => $post->slug])" :imageUrl="asset('storage/' . $post->thumbnail)" :newsTitle="$post->title" :newsTimestamp="$formattedTimestamp" />
              @endforeach
              <div class="flex w-[90%] justify-center">
                <a class="mt-2 inline-block bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                  href="{{ route('news-articles.all') }}" wire:navigate>View All News</a>
              </div>
            </div>
          </div>
          <div class="w-[35%] border-l-8 border-white">
            <div class="w-full">
              <div class="w-full border-b-4 border-white">
                <span class="-ml-2 bg-white px-2 py-0.5 text-2xl font-semibold text-green-800">Stay Connected</span>
              </div>
              <div class="border-b-4 border-white p-6">
                <p class="sports-night-ns text-2xl font-bold tracking-wider">Follow us on </p>
                <a class="group mt-6 flex h-[60px] w-full items-center gap-4" href="">
                  <span class="inline-flex h-full w-1/5 items-center justify-center bg-[#4267B2]">
                    <svg
                      class="feather feather-facebook duration-200 group-hover:fill-white group-hover:stroke-[#6f8ece]"
                      xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                      fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                      stroke-linejoin="round">
                      <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                    </svg>
                  </span>
                  <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Facebook</span>
                </a>

                <a class="group mt-4 flex h-[60px] w-full items-center gap-4" href="">
                  <span class="inline-flex h-full w-1/5 items-center justify-center bg-[#1DA1F2]">
                    <svg
                      class="feather feather-twitter duration-200 group-hover:fill-white group-hover:stroke-[#3fb6ff]"
                      xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                      fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                      stroke-linejoin="round">
                      <path
                        d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z">
                      </path>
                    </svg>
                  </span>
                  <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Twitter</span>
                </a>

                <a class="group mt-4 flex h-[60px] w-full items-center gap-4" href="">
                  <span
                    class="inline-flex h-full w-1/5 items-center justify-center duration-200 group-hover:opacity-80"
                    id="instagram-icon">
                    <svg
                      class="feather feather-instagram duration-200 group-hover:fill-white group-hover:stroke-pink-600"
                      xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                      fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                      stroke-linejoin="round">
                      <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                      <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                      <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                    </svg>
                  </span>
                  <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Instagram</span>
                </a>

              </div>
              <div class="p-6">
                <p class="sports-night-ns text-2xl font-semibold tracking-wider">News categories</p>
                <ul class="grid w-full grid-cols-1 justify-between gap-x-6 text-xl font-semibold">
                  <a class="mt-4 inline-block duration-150 hover:text-gray-300"
                    href="{{ route('news-articles.all') }}" wire:navigate>
                    <li>All Sport News</li>
                  </a>
                  @foreach ($categories as $category)
                    <a class="mt-4 inline-block duration-150 hover:text-gray-300"
                      href="{{ route('news-articles.categories', ['category' => $category->slug]) }}" wire:navigate>
                      <li>{{ $category->title }}</li>
                    </a>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-full overflow-clip px-10 pb-10">
      <p class="sports-night mt-6 py-6 text-5xl font-bold">Featured Pitches / Highest Ratings</p>
      <div class="swiper-container relative h-[500px] w-full">
        <div class="swiper-wrapper flex h-full w-full gap-2">
          @forelse ($pitches as $pitch)
            @livewire('pitch-card-md', ['pitch' => $pitch])

          @empty
            <div>
              No Pitches available in our website yet!
            </div>
          @endforelse
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>

      </div>
    </div>
    <div class="w-full border-t-8 border-white pb-16">
      <h2 class="sports-night py-6 text-center text-5xl font-bold">Testimonials</h2>
      <div class="animated fadeInDown mx-auto flex w-[90%]">
        <div class="grid w-[70%] grid-cols-12 gap-10">

          @foreach ($testimonials as $single_testimonial)
            <div
              class="group relative col-span-6 h-[300px] overflow-clip border-4 border-white shadow-2xl duration-200 hover:ring hover:ring-white">
              @if ($single_testimonial->user->profile_picture)
                <img
                  class="absolute right-20 top-2 aspect-square w-1/4 rounded-full border-4 border-white object-cover shadow-2xl duration-200 group-hover:translate-x-4"
                  src="{{ asset('storage/' . $single_testimonial->user->profile_picture) }}" alt="">
              @else
                @php
                  $firstName = explode(' ', $single_testimonial->user->name)[0];
                  $firstLetter = strtoupper(substr($firstName, 0, 1));
                @endphp
                <div
                  class="absolute right-20 top-2 flex aspect-square w-1/4 items-center justify-center rounded-full border-4 border-white bg-green-600 text-6xl font-thin shadow-2xl duration-200 group-hover:translate-x-4"
                  style="background-image: url('{{ asset('images/football-no-lines.png') }}')">
                  {{ $firstLetter }}</div>
              @endif
              <div class="absolute bottom-0 left-0 h-3/5 w-full skew-y-[15deg] bg-white"></div>
              <div class="absolute bottom-0 left-1/3 h-3/5 w-full -skew-y-[8deg] bg-white"></div>
              <div class="absolute bottom-0 left-0 flex h-[55%] w-full flex-col justify-between bg-white px-4 pb-3">
                <p class='font-serif text-lg font-semibold text-amber-400'>{{ $single_testimonial->user->name }} said:
                </p>
                <p class="text-green-800">"{{ $single_testimonial->content }}"</p>
              </div>
            </div>
          @endforeach

        </div>
        <div class="w-[30%] pl-6">
          <p class="text-5xl font-black text-amber-400" id="testiomial-call-to-action">Only four of many!</p>
          <div class="mt-8 border-l-4 border-t-4 border-white p-4 text-center text-lg font-semibold">
            <p class='text-xl'>Are you a happy customer yourself?</p>
            <p class="mt-3"> Give us
              your review of our app.</p>
            <a class="mt-3 inline-block bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
              href="{{ route('review') }}" wire:navigate>Click Here!</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="flex w-full items-end justify-between bg-white px-8 pb-2 pt-3 text-green-800">
    <div class="w-[30%]">
      <a class='text-2xl font-bold duration-150 hover:text-green-600' href="">Pitchify</a>
    </div>
    <div class="flex w-[40%] justify-center">
      <p class="text-lg font-semibold">&#169;2024 Pitchify&trade;. All rights reserved</p>
    </div>
    <div class="flex w-[30%] items-center justify-end gap-4">
      <svg class="feather feather-facebook duration-200 group-hover:fill-white group-hover:stroke-[#6f8ece]"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
      </svg>
      <svg class="feather feather-twitter duration-200 group-hover:fill-white group-hover:stroke-[#3fb6ff]"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <path
          d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z">
        </path>
      </svg>
      <svg class="feather feather-instagram duration-200 group-hover:fill-white group-hover:stroke-pink-600"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
        <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
        <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
      </svg>
    </div>
  </footer>
  <script src="{{ asset('swiper/swiper-bundle.js') }}"></script>
  <script>
    var mySwiper = new Swiper('.swiper-container', {
      slidesPerView: 2.6,
      spaceBetween: 10,

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        640: {
          slidesPerView: 1, // Number of slides on smaller screens
        },
        768: {
          slidesPerView: 2, // Number of slides on medium screens
        },
        1024: {
          slidesPerView: 2.6, // Number of slides on larger screens
        },
      },
    });
  </script>
</x-guest-layout>
