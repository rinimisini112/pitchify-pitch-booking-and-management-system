<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title', 'Default Title')</title>

  <!-- Fonts -->
  <link href="https://fonts.bunny.net" rel="preconnect">
  <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

  <!-- Scripts -->
  @vite(['resources/css/app.css', 'resources/js/app.js'])
  <script src="{{ asset('gsap/dist/gsap.min.js') }}"></script>
  <link href="{{ asset('customs.css') }}" rel="stylesheet">
  @yield('swiper-css')

</head>

<body class="relative font-sans text-white antialiased">
  <div class="min-h-screen bg-green-800 pt-6 sm:justify-center sm:pt-0"
    style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')">
    {{ $slot }}
  </div>

</body>

</html>
