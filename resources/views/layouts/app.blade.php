<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title', 'Default Title')</title>

  <!-- Fonts -->
  <link href="https://fonts.bunny.net" rel="preconnect">
  <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

  <style>
    [x-cloak] {
      display: none !important;
    }
  </style>
  @livewireStyles
  @filamentStyles
  @vite(['resources/css/app.css', 'resources/js/app.js'])
  <link href="{{ asset('customs.css') }}" rel="stylesheet">
  @yield('swiper-css')
</head>

<body class="font-sans antialiased">

  <div class="min-h-screen bg-green-700" style="background-image: url('{{ asset('images/soft-wallpaper(1).png') }}')">
    <livewire:layout.navigation />

    <!-- Page Content -->
    <main>
      @yield('content')
    </main>
  </div>
  <x-app-footer></x-app-footer>
  @livewireScripts
  @filamentScripts
</body>

</html>
