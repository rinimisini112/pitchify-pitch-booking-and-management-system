@props(['active'])

@php
  $classes = $active ?? false ? 'inline-flex items-center px-1 pt-2 border-b-4 border-amber-400 dark:border-amber-600 text-lg font-medium leading-5 text-gray-900 dark:text-gray-100 focus:outline-none focus:border-amber-700 transition duration-150 ease-in-out' : 'inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-base font-medium leading-5 text-gray-300  hover:border-amber-300  focus:outline-none focus:text-gray-700  focus:border-gray-300  transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
  {{ $slot }}
</a>
