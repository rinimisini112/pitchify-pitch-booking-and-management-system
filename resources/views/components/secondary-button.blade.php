<button
  {{ $attributes->merge(['type' => 'button', 'class' => 'bg-white px-2 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8']) }}>
  {{ $slot }}
</button>
