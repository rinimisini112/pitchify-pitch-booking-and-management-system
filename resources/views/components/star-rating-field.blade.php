@props(['rating' => 0, 'size' => 8])

@php
  $filledColor = '#FDCC0D';
  $emptyColor = 'none';
@endphp

<div class="flex">
  <svg class="w-{{ $size }} h-{{ $size }}" style="enable-background:new 0 0 45 43;" version="1.1"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 45 43"
    xml:space="preserve">
    @if ($rating >= 0.8)
      <polygon style="fill:{{ $filledColor }};stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @elseif ($rating < 0.8 && $rating >= 0.3)
      <polygon class="half" style="fill:{{ $filledColor }};"
        points="3.5,17.5 16.5,15.5 22.5,3.5 22.5,33.5 10.5,39.5 12.5,26.5 " />
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
  22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @else
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @endif
  </svg>

  <svg class="w-{{ $size }} h-{{ $size }}" style="enable-background:new 0 0 45 43;" version="1.1"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 45 43"
    xml:space="preserve">
    @if ($rating >= 1.8)
      <polygon style="fill:{{ $filledColor }};stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @elseif ($rating < 1.8 && $rating >= 1.3)
      <polygon class="half" style="fill:{{ $filledColor }};"
        points="3.5,17.5 16.5,15.5 22.5,3.5 22.5,33.5 10.5,39.5 12.5,26.5 " />
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
  22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @else
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @endif
  </svg>
  <svg class="w-{{ $size }} h-{{ $size }}" style="enable-background:new 0 0 45 43;" version="1.1"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 45 43"
    xml:space="preserve">
    @if ($rating >= 2.8)
      <polygon style="fill:{{ $filledColor }};stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @elseif ($rating < 2.8 && $rating >= 2.3)
      <polygon class="half" style="fill:{{ $filledColor }};"
        points="3.5,17.5 16.5,15.5 22.5,3.5 22.5,33.5 10.5,39.5 12.5,26.5 " />
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
  22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @else
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @endif
  </svg>
  <svg class="w-{{ $size }} h-{{ $size }}" style="enable-background:new 0 0 45 43;" version="1.1"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 45 43"
    xml:space="preserve">
    @if ($rating >= 3.8)
      <polygon style="fill:{{ $filledColor }};stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @elseif ($rating < 3.8 && $rating >= 3.3)
      <polygon class="half" style="fill:{{ $filledColor }};"
        points="3.5,17.5 16.5,15.5 22.5,3.5 22.5,33.5 10.5,39.5 12.5,26.5 " />
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
  22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @else
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @endif
  </svg>
  <svg class="w-{{ $size }} h-{{ $size }}" style="enable-background:new 0 0 45 43;" version="1.1"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 45 43"
    xml:space="preserve">
    @if ($rating >= 4.8)
      <polygon style="fill:{{ $filledColor }};stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @elseif ($rating < 4.8 && $rating >= 4.3)
      <polygon class="half" style="fill:{{ $filledColor }};"
        points="3.5,17.5 16.5,15.5 22.5,3.5 22.5,33.5 10.5,39.5 12.5,26.5 " />
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
  22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @else
      <polygon style="fill:none;stroke:white;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
        points="2,16.9 16.2,14.9 
22.3,2.5 28.8,14.9 43,16.9 33,26.6 35.1,41 22.5,34.3 9.9,41 12.3,26.9 " />
    @endif
  </svg>
</div>
