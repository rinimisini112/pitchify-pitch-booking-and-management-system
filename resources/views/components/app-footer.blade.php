<footer class="flex w-full items-end justify-between bg-white px-11 text-green-800">
  <div class="flex w-[70%] items-center border-r-8 border-green-800 py-3">
    <div class="w-2/5">
      <p class="w-full border-b-4 border-b-green-800 pb-2 pl-2">
        <a class='text-4xl font-bold duration-150 hover:text-green-600' href="">Pitchify</a> <span
          class="text-sm">"Your virtual sport assistant"</span>
      </p>
      <p class="mt-4 pl-2 text-lg font-semibold">&#169;2024 Pitchify&trade;. All rights reserved</p>
    </div>
    <div class="w-3/5">
      <ul class="flex items-center justify-center gap-5 text-lg">
        <a class="duration-100 ease-in hover:text-green-500" href="">
          <li>Dashboard</li>
        </a>
        <a class="duration-100 ease-in hover:text-green-500" href="">
          <li>Pitches</li>
        </a>
        <a class="duration-100 ease-in hover:text-green-500" href="">
          <li>News</li>
        </a>
        <a class="duration-100 ease-in hover:text-green-500" href="">
          <li>My Reservations</li>
        </a>
      </ul>
    </div>
  </div>
  <div class="flex w-[30%] flex-col items-center py-3">
    <p class="pb-4 text-lg">Follow us on social media</p>
    <div class="flex items-center justify-center gap-4">
      <svg class="feather feather-facebook duration-200 group-hover:fill-white group-hover:stroke-[#6f8ece]"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
      </svg>
      <svg class="feather feather-twitter duration-200 group-hover:fill-white group-hover:stroke-[#3fb6ff]"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <path
          d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z">
        </path>
      </svg>
      <svg class="feather feather-instagram duration-200 group-hover:fill-white group-hover:stroke-pink-600"
        xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none"
        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
        <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
        <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
        <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
      </svg>
    </div>
  </div>
</footer>
