@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-md text-amber-400']) }}>
  {{ $value ?? $slot }}
</label>
