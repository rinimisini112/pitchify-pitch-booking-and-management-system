<a
  {{ $attributes->merge(['class' => 'block w-full px-4 py-2 text-start text-sm leading-5 text-green-800 hover:bg-green-800 focus:outline-none focus:bg-green-800 focus:text-white hover:text-white transition duration-150 ease-in-out']) }}>{{ $slot }}</a>
