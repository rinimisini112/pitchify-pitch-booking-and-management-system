<button
  {{ $attributes->merge(['type' => 'submit', 'class' => 'bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8']) }}>
  {{ $slot }}
</button>
