<div class="sticky top-0 z-30 w-full bg-green-700 px-10 py-4 text-white shadow-2xl drop-shadow-lg"
  style="background-image:url('{{ asset('images/football-no-lines.png') }}')">
  <div>
    <a class="text-4xl font-semibold tracking-wider" href="{{ route('home') }}">Pitchify</a>
    <span class="pl-4 text-lg">"Your virtual sport assistant"</span>
  </div>
  <div>
    @livewire('welcome.navigation')
  </div>
</div>
