@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge([
    'class' => 'border-amber-300 border-white  focus:border-amber-500 focus:ring-amber-500 text-green-800 shadow-sm',
]) !!}>
