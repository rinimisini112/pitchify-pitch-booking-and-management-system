@props(['imageUrl', 'newsTitle', 'newsTimestamp'])

<a class="group mb-6 inline-flex h-[200px] w-[95%] border-4 border-white duration-200 hover:ring hover:ring-white"
  href="{{ $articleLink }}" wire:navigate>
  <div class="h-full w-1/2 overflow-clip border-r-4 border-white">
    <img class="h-full w-full object-cover duration-200 group-hover:scale-110" src="{{ $imageUrl }}" alt="">
  </div>
  <div class="flex w-1/2 flex-col justify-between px-4 pb-1 pt-4">
    <p class="text-2xl font-bold">{{ $newsTitle }}</p>
    <p class="place-self-end text-lg font-bold text-amber-400">{{ $newsTimestamp }}</p>
  </div>
</a>
