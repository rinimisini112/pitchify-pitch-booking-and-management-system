@section('title', 'Update your Profile')
<style>
  .fi-input {
    border: 1px solid #ffc107 !important;
    border-radius: 0 !important;
  }

  .fi-input-wrp {
    border-radius: 0 !important;
  }

  .fi-fo-wizard {
    background-color: transparent !important;
    border: 3px solid white !important;
    border-radius: 0 !important;
  }

  .fi-fo-wizard-header-step-label {
    color: white !important;
  }

  .fi-fo-wizard-header-step-icon-ctn {
    color: #ffc107;
  }

  .fi-fo-field-wrp-error-message {
    color: white !important;
    padding-left: 2px;
    padding-right: 3px;
    padding-block: 2px;
  }
</style>
@extends('layouts.app')
@section('content')
  <x-slot name="header">
    <h2 class="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
      {{ __('Profile') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="mx-auto max-w-6xl space-y-6 sm:px-6 lg:px-8">
      @livewire('user-profile-form', [auth()->user()])
      @livewire('user-password-wizard')
      <div class="border-4 border-white p-4 shadow sm:p-8">
        <div class="max-w-xl">
          <livewire:profile.delete-user-form />
        </div>
      </div>
    </div>
  </div>
@endsection
