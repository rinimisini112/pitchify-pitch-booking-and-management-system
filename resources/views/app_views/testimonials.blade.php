@section('title', 'Pitchify Give us a Review')

@extends('layouts.app')

@section('content')
  <div class="pt-12 text-white">
    <div class="max-w-8xl mx-auto flex items-center justify-center sm:px-6 lg:px-8">
      @livewire('testimonial-form')
    </div>
  </div>
  </div>
@endsection
