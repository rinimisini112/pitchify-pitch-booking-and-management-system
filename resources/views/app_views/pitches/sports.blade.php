@section('title', 'Pitchify all sports')

@extends('layouts.app')

@section('content')
  <div class="mb-5 pt-12 text-white">
    <div class="max-w-8xl mx-auto mb-8 sm:px-6 lg:px-8">
      <div class="flex w-full items-center justify-between bg-white px-12 py-6 text-2xl text-green-800 shadow-2xl"
        id="animateSmth">
        <p>Pitchify all Sports</p>
        <div class="flex items-center gap-6 text-lg">
          <a class="duration-150 ease-in hover:text-green-500" href="{{ route('pitches.index') }}" wire:navigate>Back to
            Pitches</a>
          <a class="duration-150 ease-in hover:text-green-500" href="">View News</a>
        </div>
      </div>
      @livewire('search-all-sports')
    </div>
  </div>
@endsection
