@section('title', 'Pitchify Our Pitches')
@extends('layouts.app')

@section('content')
  <div class="pt-12 text-white">
    <div class="max-w-8xl animated fadeInDown mx-auto sm:px-6 lg:px-8">
      <div class="w-full bg-white px-4 py-6 text-center text-2xl text-green-800 shadow-2xl">
        <p>Pitchify all available Pitches <span class="ms-12">Click here to view all <a
              class="ms-2 inline-block border-4 border-green-800 bg-white px-4 py-1 text-lg font-bold tracking-wide text-green-800 duration-150 hover:bg-green-800 hover:text-white hover:ring hover:ring-green-800 active:ring-8"
              href="{{ route('pitches.allSports') }}" wire:navigate>Sports listed</a></span></p>
      </div>
      <div class="mx-auto flex w-[95%]">
        <div class="w-[70%] py-8">
          <div class="mr-auto w-full border-b-8 border-white">
          </div>
          @livewire('pitch-container')
        </div>
        <div class="w-[30%] border-l-8 border-white py-4 text-white">
          <div class="w-full border-b-4 border-white py-4 text-center">
            @if (Auth::user()->testimonial)
              <p class="text-2xl font-medium">Thank you for writting a review</p>
              <p class="px-2 pt-2">If your opinion has changed dont hesitate to write another one!</p>
              <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="{{ route('review') }}" wire:navigate>Write another review</a>
            @else
              <p class="text-2xl font-medium">Are you happy Customer?</p>
              <p class="pt-2">Write a testimonial and give us your review!</p>
              <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="{{ route('review') }}" wire:navigate>Give us a rating</a>
            @endif
          </div>
          @livewire('sports-sidebar', [$sports])

        </div>
      </div>
    </div>
  </div>
@endsection
