@section('title', 'Pitchify Reserve ' . $pitch->title)
@section('swiper-css')
  <link href="{{ asset('swiper/swiper-bundle.css') }}" rel="stylesheet">
@endsection
<style>
  .fi-input {
    border: 1px solid #ffc107 !important;
    border-radius: 0 !important;
    color: white !important;
  }

  .mdtp__time_holder {
    background-color: green !important;
  }

  .fi-input-wrp {
    border-radius: 0 !important;
    border: 3px solid #ffc107 !important;
    color: white !important;
  }

  #data\.start_time::placeholder {
    color: white !important;
  }

  .fi-input-wrp-label {
    color: white !important;
  }

  .fi-fo-wizard-header-step-label {
    color: white !important;
  }

  .fi-fo-field-wrp-label {
    color: white !important;
  }

  .fi-fo-field-wrp {
    color: green !important;
  }

  .fi-fo-wizard {
    background-color: transparent !important;
    backdrop-filter: blur(5px);
    border: 3px solid #ffc107 !important;
    color: green !important;
    border-radius: 0 !important;
  }

  .fi-fo-wizard-header-step-icon-ctn {
    color: #ffc107;
  }

  .fi-fo-field-wrp-error-message {
    color: red !important;
    padding-left: 2px;
    padding-right: 3px;
    padding-block: 2px;
  }

  .fi-fo-date-time-picker-panel {
    background-color: green !important;
    color: white;
    border: 4px solid white;
  }

  .fi-fo-date-time-picker-panel div select {
    background-color: white !important;
    color: green !important;
    padding-left: 2px !important;
  }

  .fi-select-input option {
    background-color: white !important;
    color: green !important;
  }
</style>
@extends('layouts.app')

@section('content')
  <div class="pt-12 text-white">

    <div class="max-w-8xl animated fadeInDown mx-auto sm:px-6 lg:px-8">
      <div class="mx-auto flex w-4/5">

        @livewire('reservation-wizard', ['pitch' => $pitch])
      </div>
    </div>
  </div>
@endsection
