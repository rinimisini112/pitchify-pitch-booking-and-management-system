@section('title', 'Pitchify: All News Articles')

@extends('layouts.news')

@section('news-content')
  <div class="animated fadeInDown w-[65%] pr-6">
    <div class="w-full border-b-4 border-white">
      <span class="bg-white px-4 py-0.5 text-2xl font-semibold text-green-800">Latest news</span>
    </div>
    @livewire('article-container')
  </div>
@endsection
