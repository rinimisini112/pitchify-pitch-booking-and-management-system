@section('title', 'Pitchify: News')

@extends('layouts.app')

@section('content')
  <div class="pt-12 text-white">
    <div class="max-w-8xl animated fadeInDown mx-auto sm:px-6 lg:px-8">

      <div class="mx-auto flex w-[95%]">
        <div class="w-full border-t-8 border-white p-4">
          <div class="mx-auto w-full p-4">
            <p class="text-center text-3xl"><span class="bg-white px-2 font-semibold text-green-800">Pitchify News!</span>
              Stay
              updated with everything
              sport related in Kosovo</p>
            <h3 class="mt-8 text-4xl font-bold">Featured</h3>
            <div class="mt-2">
              @livewire('news-headings-widget')
            </div>
            <div class="mt-8 flex w-full gap-4">
              <div class="w-[65%] pr-6">
                <div class="w-full border-b-4 border-white">
                  <span class="bg-white px-4 py-0.5 text-2xl font-semibold text-green-800">Latest news</span>
                </div>
                <div class="w-full pt-6">
                  @foreach ($posts as $post)
                    <x-news-card-small :articleLink="route('news.article.show', ['post' => $post->slug])" :imageUrl="asset('storage/' . $post->thumbnail)" :newsTitle="$post->title" :newsTimestamp="$post->published_at" />
                  @endforeach
                  <div class="flex w-[90%] justify-center">
                    <a class="mt-2 inline-block bg-white px-8 py-2 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                      href="{{ route('news-articles.all') }}" wire:navigate>View All News</a>
                  </div>
                </div>
              </div>
              <div class="w-[35%] border-l-8 border-white">

                <div class="w-full">
                  <div class="w-full border-b-4 border-white">
                    <span class="-ml-2 bg-white px-2 py-0.5 text-2xl font-semibold text-green-800">Stay Connected</span>
                  </div>
                  <div class="border-b-4 border-white p-6">
                    <p class="text-3xl font-bold">Follow us on </p>
                    <a class="group mt-6 flex h-[60px] w-full items-center gap-4" href="">
                      <span class="inline-flex h-full w-1/5 items-center justify-center bg-[#4267B2]">
                        <svg
                          class="feather feather-facebook duration-200 group-hover:fill-white group-hover:stroke-[#6f8ece]"
                          xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                          fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                          stroke-linejoin="round">
                          <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                        </svg>
                      </span>
                      <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Facebook</span>
                    </a>

                    <a class="group mt-4 flex h-[60px] w-full items-center gap-4" href="">
                      <span class="inline-flex h-full w-1/5 items-center justify-center bg-[#1DA1F2]">
                        <svg
                          class="feather feather-twitter duration-200 group-hover:fill-white group-hover:stroke-[#3fb6ff]"
                          xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                          fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                          stroke-linejoin="round">
                          <path
                            d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z">
                          </path>
                        </svg>
                      </span>
                      <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Twitter</span>
                    </a>

                    <a class="group mt-4 flex h-[60px] w-full items-center gap-4" href="">
                      <span
                        class="inline-flex h-full w-1/5 items-center justify-center duration-200 group-hover:opacity-80"
                        id="instagram-icon">
                        <svg
                          class="feather feather-instagram duration-200 group-hover:fill-white group-hover:stroke-pink-600"
                          xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24"
                          fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                          stroke-linejoin="round">
                          <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                          <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                          <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                      </span>
                      <span class="text-2xl font-semibold duration-200 group-hover:text-gray-300">Instagram</span>
                    </a>

                  </div>
                  <div class="p-6">
                    <p class="text-3xl font-semibold">News categories</p>
                    <ul class="grid w-full grid-cols-1 justify-between gap-x-6 text-xl font-semibold">
                      <a class="mt-4 inline-block duration-150 hover:text-gray-300"
                        href="{{ route('news-articles.all') }}" wire:navigate>
                        <li>All Sport News</li>
                      </a>
                      @foreach ($categories as $category)
                        <a class="mt-4 inline-block duration-150 hover:text-gray-300"
                          href="{{ route('news-articles.categories', ['category' => $category->slug]) }}" wire:navigate>
                          <li>{{ $category->title }}</li>
                        </a>
                      @endforeach
                    </ul>
                  </div>
                  <div class="w-full border-t-4 border-white pt-8 text-center">
                    @if (Auth::user()->testimonial)
                      <p class="text-2xl font-medium">Thank you for writting a review</p>
                      <p class="px-2 pt-2">If your opinion has changed dont hesitate to write another one!</p>
                      <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                        href="{{ route('review') }}" wire:navigate>Write another review</a>
                    @else
                      <p class="text-2xl font-medium">Are you happy Customer?</p>
                      <p class="pt-2">Write a testimonial and give us your review!</p>
                      <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                        href="{{ route('review') }}" wire:navigate>Give us a rating</a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
