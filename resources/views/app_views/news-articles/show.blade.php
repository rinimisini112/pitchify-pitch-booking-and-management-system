@section('title', 'Pitchify: ' . $post->title)
@php
  View::share('postCategory', $post->category);
@endphp
@extends('layouts.news')

@section('news-content')
  <div class="animated fadeInDown w-[65%] pr-6">
    <div class="w-full border-b-4 border-white">

      <div class="w-full border-4 border-white shadow-xl">
        <img src="{{ asset('storage/' . $post->thumbnail) }}" alt="">
      </div>
      <h1 class="mt-5 text-3xl font-black">{{ $post->title }}</h1>
      <div class="mt-3 flex justify-between pb-4">
        @php
          $publishedAt = \Carbon\Carbon::parse($post->published_at);
          $formattedTimestamp = $publishedAt->format('F jS Y h:i A');

          $tagsArray = explode(',', rtrim($post->tags, ','));
        @endphp
        <p>{{ $formattedTimestamp }}</p>
        <p>Category - <span class="font-bold">{{ $post->category->title }}</span></p>
      </div>
      <p class="border-b-4 border-white pb-4">Tags -
        @foreach ($tagsArray as $tag)
          <span class="rounded-lg bg-white px-3 py-1 text-green-800 shadow-xl">{{ $tag }}</span>
        @endforeach
      </p>
      <div class="mt-4 pb-4" id="news-article-content-box">
        {!! $post->content !!}
      </div>
      <div class="mt-2 flex justify-end">
        <p class="text-xl">Interested in seeing more?<a
            class="ms-3 bg-white px-6 py-1.5 font-bold uppercase tracking-wide text-green-700 duration-150 hover:border-x-4 hover:border-t-4 hover:border-white hover:bg-transparent hover:text-white"
            href="{{ route('news-articles.categories', ['category' => $post->category->slug]) }}" wire:navigate>Click
            Here</a>
        </p>
      </div>
    </div>
  </div>
@endsection
