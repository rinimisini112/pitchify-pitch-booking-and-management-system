@section('title', 'Pitchify Home')

@extends('layouts.app')
@section('content')
  <div class="pt-12 text-white">
    @livewire('notification-popup')
    <div class="animated fadeInDown mx-auto max-w-[83rem] sm:px-6 lg:px-8">
      <div class="w-full bg-white px-4 py-6 text-center text-2xl text-green-800 shadow-2xl">
        <p>You have no reservations yet! Make your first one now <a
            class="ms-5 border-4 border-green-800 px-6 py-2 text-lg duration-200 hover:bg-green-800 hover:text-white"
            href="">Start Booking</a></p>
        <div class="mt-5 border-t-4 border-green-800 pt-4">
          <p class="font-bold">Your Statistics</p>
          <div class="flex justify-between px-12 pt-4 text-lg">
            <span class="bg-green-800 px-3 py-1.5 text-white">Total Pitches Reserved : 12</span>
            <span class="bg-green-800 px-3 py-1.5 text-white">Total Minutes Played : 720 </span>
            <span class="bg-green-800 px-3 py-1.5 text-white">Most Booked Pitch : Olympic Stadium Jezerc </span>
          </div>
        </div>
      </div>
      <div class="mx-auto flex w-[95%]">
        <div class="w-[70%] py-8">
          <div class="mr-auto w-full border-b-8 border-white">
          </div>
          <div class="w-full px-10 py-4">
            <h1 class="text-3xl font-semibold">Most Booked at the moment</h1>
            @livewire('pitch-card-big', [$pitches[0]])
            <h1 class="mt-6 border-t-8 border-white pt-2 text-3xl font-semibold">You might also like</h1>
            @livewire('pitch-card-big', [$pitches[1]])
            @livewire('pitch-card-big', [$pitches[2]])
          </div>

        </div>
        <div class="w-[30%] border-l-8 border-white py-4 text-white">
          <div class="w-full border-b-4 border-white py-4 text-center">
            @if (Auth::user()->testimonial)
              <p class="text-2xl font-medium">Thank you for writting a review</p>
              <p class="px-2 pt-2">If your opinion has changed dont hesitate to write another one!</p>
              <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="{{ route('review') }}" wire:navigate>Write another review</a>
            @else
              <p class="text-2xl font-medium">Are you happy Customer?</p>
              <p class="pt-2">Write a testimonial and give us your review!</p>
              <a class="mt-4 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="{{ route('review') }}" wire:navigate>Give us a rating</a>
            @endif
          </div>
          @livewire('sports-sidebar', [$sports])
          <div class="mt-6 border-t-4 border-white text-center">
            <h1 class="pt-4 text-3xl font-bold">Pitchify Sport News!</h1>
            <div class="mt-2">
              @foreach ($posts as $post)
                <a class="relative ml-auto mt-4 inline-block w-[90%] border-4 border-white duration-150 hover:ring hover:ring-white"
                  href="{{ route('news.article.show', ['post' => $post->slug]) }}" wire:navigate>
                  <img class="block h-[220px] max-h-[220px] w-full object-cover"
                    src="{{ asset('storage/' . $post->thumbnail) }}" alt="">
                  <div
                    class="absolute left-0 top-0 flex h-full w-full flex-col justify-between bg-gradient-to-t from-[#2e7d32c5] via-transparent to-green-800 p-0.5 pl-1 text-left duration-150 hover:opacity-0">
                    @if (Str::length($post->title) > 60)
                      <p class="text-xl font-bold">{{ $post->title = Str::limit($post->title, 57, '...') }}</p>
                    @else
                      <p class="text-xl font-bold">{{ $post->title }}</p>
                    @endif
                    <p>{{ $post->formatted_published_at }}</p>
                  </div>
                </a>
              @endforeach
              <p class="mt-4 text-lg font-semibold">View all News Articles!</p>
              <a class="mt-2 inline-block bg-white px-6 py-1.5 font-bold tracking-wide text-green-700 duration-150 hover:bg-transparent hover:text-white hover:ring hover:ring-white active:ring-8"
                href="{{ route('news-articles.all') }}" wire:navigate>Click Here!</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
